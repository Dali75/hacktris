# Hacktris Game

### Synopsis
Welcome to my game of Tetris, a game I love to play. It's a Spring boot / Java FX / JAVA 21 game.

Currently, two languages are supported :
- French
- English

Basics features are implemented for playing.
It is possible to hold a tetrimino, to pause the game. The duration time is also displayed.

Hope you'll like it.

### How to play ?

- Launch the jar file.
- On the main window dialog, click on "play" then "start" to begin a game.
- Use the arrow keys, "left", "right", to move the tetriminos left or right.
- The "up" arrow key is for the tetrimino rotation.
- The "down" arrow is to accelerate the fall of the tetrimino.
- The "space" key is used to make a hard drop of the tetrimino.
- The "p" key is used to pause the game or to resume it.
- The "h" key is used to hold a tetrimino or to bring it back on the main grid.