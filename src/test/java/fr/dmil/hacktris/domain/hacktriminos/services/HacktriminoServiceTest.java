package fr.dmil.hacktris.domain.hacktriminos.services;

import static org.assertj.core.api.Assertions.assertThat;

import fr.dmil.hacktris.JavaFXIntegrationTest;
import fr.dmil.hacktris.domain.hacktriminos.models.HacktriminoZ;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class HacktriminoServiceTest extends JavaFXIntegrationTest {
    @Autowired private HacktriminoService hacktriminoService;

    @Test
    void should_generate_a_hacktrimino_of_same_type_and_3_0_coordinates() {
        // Data
        var hacktrimino = new HacktriminoZ(1, 1);
        var hacktriminoHeld = hacktriminoService.generateHeldForHacktrisGrid(hacktrimino);

        // Assertions
        assertThat(hacktriminoHeld).isInstanceOf(HacktriminoZ.class);
        assertThat(hacktriminoHeld).extracting("x", "y").containsExactly(3, 0);
    }

    @Test
    void should_generate_the_next_hacktrimino_of_same_type_and_0_0_coordinates() {
        // Data
        var hacktrimino = new HacktriminoZ(1, 1);
        var nextHacktrimino = hacktriminoService.generateNext(hacktrimino);

        // Assertions
        assertThat(nextHacktrimino).isInstanceOf(HacktriminoZ.class);
        assertThat(nextHacktrimino).extracting("x", "y").containsExactly(0, 0);
    }

    @Test
    void should_generate_a_held_hacktrimino_of_same_type_and_0_0_coordinates() {
        // Data
        var hacktrimino = new HacktriminoZ(1, 1);
        var hacktriminoHeld = hacktriminoService.generateHeld(hacktrimino);

        // Assertions
        assertThat(hacktriminoHeld).isInstanceOf(HacktriminoZ.class);
        assertThat(hacktriminoHeld).extracting("x", "y").containsExactly(0, 0);
    }
}
