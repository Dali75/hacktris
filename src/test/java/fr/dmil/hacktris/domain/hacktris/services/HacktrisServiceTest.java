package fr.dmil.hacktris.domain.hacktris.services;

import static fr.dmil.hacktris.domain.hacktris.enums.HacktrisStatus.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import fr.dmil.hacktris.JavaFXIntegrationTest;
import fr.dmil.hacktris.domain.grids.main.services.HacktrisMainGridService;
import fr.dmil.hacktris.domain.hacktriminos.models.*;
import fr.dmil.hacktris.domain.hacktriminos.services.HacktriminoService;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

class HacktrisServiceTest extends JavaFXIntegrationTest {

    @Autowired private HacktrisService hacktrisService;
    @Autowired private HacktrisMainGridService hacktrisMainGridService;
    @MockBean private HacktriminoService hacktriminoService;

    @BeforeEach
    void setUp() {
        setField(hacktrisService, "hacktrisMainGridService", hacktrisMainGridService);
    }

    @Test
    void should_start() {
        // When
        when(hacktriminoService.generateNext(any())).thenReturn(new HacktriminoO(3, 3));

        // Call
        hacktrisService.start();

        // Assertions
        assertThat(hacktrisService.isGamePaused()).isFalse();
        assertThat(hacktrisService.isGameRunning()).isTrue();

        // Verify
        verify(hacktriminoService, times(2)).generate();

        // Clean
        hacktrisService.stop();
    }

    @Test
    void should_stop() {
        // Call
        hacktrisService.stop();

        // Assertions
        assertThat(hacktrisService.isGamePaused()).isFalse();
        assertThat(hacktrisService.isGameRunning()).isFalse();
    }

    @Test
    void should_playFallSound() {
        assertThat(hacktrisService.shouldPlayFallSound(NEW_HACKTRIMINO)).isTrue();
        assertThat(hacktrisService.shouldPlayFallSound(LINES_MADE)).isTrue();
    }

    @Test
    void should_not_playFallSound() {
        assertThat(hacktrisService.shouldPlayFallSound(GAME_ALIVE)).isFalse();
    }

    @Test
    void should_pause() {
        // When
        when(hacktriminoService.generateNext(any())).thenReturn(new HacktriminoO(3, 3));

        // Start
        hacktrisService.start();

        // Call
        hacktrisService.pauseOrResume();

        // Assertion
        assertThat(hacktrisService.isGamePaused()).isTrue();

        // Clean
        hacktrisService.stop();
    }

    @Test
    void should_resume() {
        // When
        when(hacktriminoService.generateNext(any())).thenReturn(new HacktriminoO(3, 3));

        // Start
        hacktrisService.start();

        // Call
        hacktrisService.pauseOrResume();
        hacktrisService.pauseOrResume();

        // Assertion
        assertThat(hacktrisService.isGamePaused()).isFalse();

        // Clean
        hacktrisService.stop();
    }

    @Test
    void should_hold() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);
        var hacktriminoO = new HacktriminoO(3, 3);

        // When
        when(hacktriminoService.generateNext(any())).thenReturn(new HacktriminoO(3, 3));
        when(hacktriminoService.generate()).thenReturn(hacktrimino);
        when(hacktriminoService.generateHeld(any())).thenReturn(hacktriminoO);

        // Start
        hacktrisService.start();

        // Call
        hacktrisService.hold();

        // Verify
        verify(hacktriminoService, times(3)).generate();
        verify(hacktriminoService, times(1)).generateHeld(any());

        // Clean
        hacktrisService.stop();
    }

    @Test
    void should_not_hold() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);
        var hacktriminoO = new HacktriminoO(3, 3);

        // When
        when(hacktriminoService.generate()).thenReturn(hacktrimino);
        when(hacktriminoService.generateHeld(any())).thenReturn(hacktriminoO);
        when(hacktriminoService.generateNext(any())).thenReturn(hacktrimino);

        // Start
        hacktrisService.start();

        // Call
        hacktrisService.hold();
        hacktrisService.hold();

        // Verify
        verify(hacktriminoService, times(3)).generate();
        verify(hacktriminoService, times(1)).generateHeld(any());

        // Clean
        hacktrisService.stop();
    }

    @ParameterizedTest
    @MethodSource("hacktriminoProvider")
    void should_down(Hacktrimino hacktrimino) {
        // When
        when(hacktriminoService.generate()).thenReturn(hacktrimino);
        when(hacktriminoService.generateNext(any())).thenReturn(hacktrimino);

        // Start
        hacktrisService.start();

        // Assertion
        assertThat(hacktrisService.down()).isEqualTo(GAME_ALIVE);

        // Clean
        hacktrisService.stop();
    }

    @Test
    void should_hardDrop() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);

        // When
        when(hacktriminoService.generateNext(any())).thenReturn(new HacktriminoO(3, 3));
        when(hacktriminoService.generate()).thenReturn(hacktrimino);

        // Start
        hacktrisService.start();

        // Assertion
        assertThat(hacktrisService.hardDrop()).isEqualTo(NEW_HACKTRIMINO);

        // Clean
        hacktrisService.stop();
    }

    @Test
    void should_left() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);
        var hacktrisGridService = mock(HacktrisMainGridService.class);
        setField(hacktrisService, "hacktrisMainGridService", hacktrisGridService);

        // When
        when(hacktriminoService.generateNext(any())).thenReturn(new HacktriminoO(3, 3));
        when(hacktriminoService.generate()).thenReturn(hacktrimino);

        // Start
        hacktrisService.start();

        // Call
        hacktrisService.left();

        // Verify
        verify(hacktrisGridService, times(1)).goLeft(hacktrimino);

        // Clean
        hacktrisService.stop();
    }

    @Test
    void should_right() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);
        var hacktrisGridService = mock(HacktrisMainGridService.class);
        setField(hacktrisService, "hacktrisMainGridService", hacktrisGridService);

        // When
        when(hacktriminoService.generateNext(any())).thenReturn(new HacktriminoO(3, 3));
        when(hacktriminoService.generate()).thenReturn(hacktrimino);

        // Start
        hacktrisService.start();

        // Call
        hacktrisService.right();

        // Verify
        verify(hacktrisGridService, times(1)).goRight(hacktrimino);

        // Clean
        hacktrisService.stop();
    }

    @Test
    void should_rotate() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);
        var hacktrisGridService = mock(HacktrisMainGridService.class);
        setField(hacktrisService, "hacktrisMainGridService", hacktrisGridService);

        // When
        when(hacktriminoService.generateNext(any())).thenReturn(new HacktriminoO(3, 3));
        when(hacktriminoService.generate()).thenReturn(hacktrimino);

        // Start
        hacktrisService.start();

        // Call
        hacktrisService.rotate();

        // Verify
        verify(hacktrisGridService, times(1)).rotate(hacktrimino);

        // Clean
        hacktrisService.stop();
    }

    static Stream<Arguments> hacktriminoProvider() {
        return Stream.of(
                Arguments.of(new HacktriminoI(3, 3)),
                Arguments.of(new HacktriminoZ(3, 3)),
                Arguments.of(new HacktriminoJ(3, 3)),
                Arguments.of(new HacktriminoL(3, 3)),
                Arguments.of(new HacktriminoO(3, 3)),
                Arguments.of(new HacktriminoS(3, 3)),
                Arguments.of(new HacktriminoT(3, 3)));
    }
}
