package fr.dmil.hacktris.domain.grids.auxiliary.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.util.ReflectionTestUtils.getField;

import fr.dmil.hacktris.JavaFXIntegrationTest;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.impl.HacktrisHoldGridVBox;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.impl.HacktrisNextGridVBox;
import fr.dmil.hacktris.domain.grids.models.HacktrisGrid;
import fr.dmil.hacktris.domain.hacktriminos.models.HacktriminoO;
import java.util.Objects;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.lang.NonNull;

class HacktrisAuxiliaryGridServiceTest extends JavaFXIntegrationTest {

    @Autowired
    @Qualifier("hacktrisNextGridService") private HacktrisAuxiliaryGridService hacktrisNextGridService;

    @Autowired
    @Qualifier("holdHacktriminoGridService") private HacktrisAuxiliaryGridService hacktrisHoldGridService;

    @MockBean private HacktrisHoldGridVBox hacktrisHoldGridVBox;
    @MockBean private HacktrisNextGridVBox hacktrisNextGridVBox;

    @Test
    void should_add_hacktrimino_to_next_grid() {
        // Data
        var hacktrimino = new HacktriminoO(0, 0);

        var grid = getGrid(hacktrisNextGridService);

        // Call
        hacktrisNextGridService.add(hacktrimino);
        assertThat(grid.getCell(0, 1).getCode()).isEqualTo("O");
        assertThat(grid.getCell(0, 2).getCode()).isEqualTo("O");
        assertThat(grid.getCell(1, 1).getCode()).isEqualTo("O");
        assertThat(grid.getCell(1, 2).getCode()).isEqualTo("O");

        verify(hacktrisNextGridVBox, times(2)).draw(any());
    }

    @Test
    void should_reset_next_grid() {
        // Call
        hacktrisNextGridService.reset();

        // Assertions
        var grid = getGrid(hacktrisNextGridService);
        for (int y = 0; y < grid.getMaxLines(); y++) {
            for (int x = 0; x < grid.getMaxColumns(); x++) {
                assertThat(grid.getCell(y, x)).isNull();
            }
        }
    }

    @Test
    void should_add_hacktrimino_at_given_coordinates_to_hold_grid() {
        // Data
        var hacktrimino = new HacktriminoO(0, 0);

        var grid = getGrid(hacktrisHoldGridService);

        // Call
        hacktrisHoldGridService.add(hacktrimino);
        assertThat(grid.getCell(0, 1).getCode()).isEqualTo("O");
        assertThat(grid.getCell(0, 2).getCode()).isEqualTo("O");
        assertThat(grid.getCell(1, 1).getCode()).isEqualTo("O");
        assertThat(grid.getCell(1, 2).getCode()).isEqualTo("O");

        verify(hacktrisHoldGridVBox, times(2)).draw(any());
    }

    @Test
    void should_reset_hold_grid() {
        // Call
        hacktrisHoldGridService.reset();

        // Assertions
        var grid = getGrid(hacktrisHoldGridService);
        for (int y = 0; y < grid.getMaxLines(); y++) {
            for (int x = 0; x < grid.getMaxColumns(); x++) {
                assertThat(grid.getCell(y, x)).isNull();
            }
        }
    }

    @NonNull private HacktrisGrid getGrid(HacktrisAuxiliaryGridService service) {
        return (HacktrisGrid) Objects.requireNonNull(getField(service, "grid"));
    }
}
