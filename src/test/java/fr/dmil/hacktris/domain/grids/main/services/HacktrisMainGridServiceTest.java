package fr.dmil.hacktris.domain.grids.main.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.util.ReflectionTestUtils.getField;

import fr.dmil.hacktris.JavaFXIntegrationTest;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.main.impl.HacktrisMainGridVBox;
import fr.dmil.hacktris.domain.grids.models.HacktrisGrid;
import fr.dmil.hacktris.domain.hacktriminos.models.HacktriminoO;
import java.util.Objects;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.lang.NonNull;

class HacktrisMainGridServiceTest extends JavaFXIntegrationTest {

    @Autowired private HacktrisMainGridService hacktrisMainGridService;
    @MockBean private HacktrisMainGridVBox hacktrisMainGridVBox;

    @Test
    void should_remove_hacktrimino() {
        // Data
        var hacktrimino = new HacktriminoO(1, 0);
        hacktrisMainGridService.goDown(hacktrimino);
        var grid = getGrid();

        // Call
        hacktrisMainGridService.remove(hacktrimino);

        // Assertions
        assertThat(grid.isEmptyCell(1, 1)).isTrue();
        assertThat(grid.isEmptyCell(2, 1)).isTrue();
        assertThat(grid.isEmptyCell(1, 2)).isTrue();
        assertThat(grid.isEmptyCell(2, 2)).isTrue();

        verify(hacktrisMainGridVBox, times(3)).draw(any());
    }

    @Test
    void should_reset() {
        // Call
        hacktrisMainGridService.reset();

        // Assertions
        var grid = getGrid();
        for (int y = 0; y < grid.getMaxLines(); y++) {
            for (int x = 0; x < grid.getMaxColumns(); x++) {
                assertThat(grid.getCell(y, x).isEmptyCell()).isTrue();
            }
        }
    }

    @Test
    void should_rotate() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);

        // Call
        hacktrisMainGridService.rotate(hacktrimino);

        // Assertions
        assertThat(hacktrimino.getX()).isEqualTo(3);
        assertThat(hacktrimino.getY()).isEqualTo(3);

        // Clean
        hacktrisMainGridService.remove(hacktrimino);
    }

    @Test
    void should_not_rotate() {
        // Data
        var hacktriminoI = new HacktriminoO(3, 3);
        var hacktriminoO = new HacktriminoO(3, 2);
        hacktrisMainGridService.goDown(hacktriminoO);

        // Call
        hacktrisMainGridService.rotate(hacktriminoI);

        // Assertions
        assertThat(hacktriminoI.getY()).isEqualTo(3);
        assertThat(hacktriminoI.getX()).isEqualTo(3);

        // Clean
        hacktrisMainGridService.remove(hacktriminoO);
        hacktrisMainGridService.remove(hacktriminoI);
    }

    @Test
    void should_goLeft() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);

        // Call
        hacktrisMainGridService.goLeft(hacktrimino);

        // Assertions
        assertThat(hacktrimino.getY()).isEqualTo(3);
        assertThat(hacktrimino.getX()).isEqualTo(2);
        assertThat(hacktrimino.getPoints().get(0).getX()).isEqualTo(2);
        assertThat(hacktrimino.getPoints().get(1).getX()).isEqualTo(3);
        assertThat(hacktrimino.getPoints().get(2).getX()).isEqualTo(2);
        assertThat(hacktrimino.getPoints().get(3).getX()).isEqualTo(3);
        assertThat(hacktrimino.getPoints().get(0).getY()).isEqualTo(3);
        assertThat(hacktrimino.getPoints().get(1).getY()).isEqualTo(3);
        assertThat(hacktrimino.getPoints().get(2).getY()).isEqualTo(4);
        assertThat(hacktrimino.getPoints().get(3).getY()).isEqualTo(4);

        // Clean
        hacktrisMainGridService.remove(hacktrimino);
    }

    @Test
    void should_not_goLeft() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);
        var hacktriminoO = new HacktriminoO(3, 3);
        hacktrisMainGridService.goDown(hacktriminoO);

        // Call
        hacktrisMainGridService.goLeft(hacktrimino);

        // Assertions
        assertThat(hacktrimino.getY()).isEqualTo(3);
        assertThat(hacktrimino.getX()).isEqualTo(3);

        // Clean
        hacktrisMainGridService.remove(hacktrimino);
        hacktrisMainGridService.remove(hacktriminoO);
    }

    @Test
    void should_goRight() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);

        // Call
        hacktrisMainGridService.goRight(hacktrimino);

        // Assertions
        assertThat(hacktrimino.getY()).isEqualTo(3);
        assertThat(hacktrimino.getX()).isEqualTo(4);
        assertThat(hacktrimino.getPoints().get(0).getX()).isEqualTo(4);
        assertThat(hacktrimino.getPoints().get(1).getX()).isEqualTo(5);
        assertThat(hacktrimino.getPoints().get(2).getX()).isEqualTo(4);
        assertThat(hacktrimino.getPoints().get(3).getX()).isEqualTo(5);
        assertThat(hacktrimino.getPoints().get(0).getY()).isEqualTo(3);
        assertThat(hacktrimino.getPoints().get(1).getY()).isEqualTo(3);
        assertThat(hacktrimino.getPoints().get(2).getY()).isEqualTo(4);
        assertThat(hacktrimino.getPoints().get(3).getY()).isEqualTo(4);

        // Clean
        hacktrisMainGridService.remove(hacktrimino);
    }

    @Test
    void should_not_goRight() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);
        var hacktriminoO = new HacktriminoO(3, 2);
        hacktrisMainGridService.goDown(hacktriminoO);

        // Call
        hacktrisMainGridService.goRight(hacktrimino);

        // Assertions
        assertThat(hacktrimino.getY()).isEqualTo(3);
        assertThat(hacktrimino.getX()).isEqualTo(3);

        // Clean
        hacktrisMainGridService.remove(hacktrimino);
        hacktrisMainGridService.remove(hacktriminoO);
    }

    @Test
    void should_goDown() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);

        // Call
        var goDown = hacktrisMainGridService.goDown(hacktrimino);

        // Assertions
        assertThat(goDown).isTrue();
        assertThat(hacktrimino.getY()).isEqualTo(4);
        assertThat(hacktrimino.getX()).isEqualTo(3);
        assertThat(hacktrimino.getPoints().get(0).getX()).isEqualTo(3);
        assertThat(hacktrimino.getPoints().get(1).getX()).isEqualTo(4);
        assertThat(hacktrimino.getPoints().get(2).getX()).isEqualTo(3);
        assertThat(hacktrimino.getPoints().get(3).getX()).isEqualTo(4);
        assertThat(hacktrimino.getPoints().get(0).getY()).isEqualTo(4);
        assertThat(hacktrimino.getPoints().get(1).getY()).isEqualTo(4);
        assertThat(hacktrimino.getPoints().get(2).getY()).isEqualTo(5);
        assertThat(hacktrimino.getPoints().get(3).getY()).isEqualTo(5);

        // Clean
        hacktrisMainGridService.remove(hacktrimino);
    }

    @Test
    void should_not_goDown() {
        // Data
        var hacktrimino = new HacktriminoO(3, 3);
        var hacktriminoO = new HacktriminoO(3, 2);
        hacktrisMainGridService.goDown(hacktriminoO);

        // Call
        var goDown = hacktrisMainGridService.goDown(hacktrimino);

        // Assertions
        assertThat(goDown).isFalse();

        // Clean
        hacktrisMainGridService.remove(hacktrimino);
        hacktrisMainGridService.remove(hacktriminoO);
    }

    @NonNull private HacktrisGrid getGrid() {
        return (HacktrisGrid) Objects.requireNonNull(getField(hacktrisMainGridService, "grid"));
    }
}
