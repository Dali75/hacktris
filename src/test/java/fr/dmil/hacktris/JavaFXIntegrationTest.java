package fr.dmil.hacktris;

import fr.dmil.hacktris.application.Launcher;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import javafx.application.Platform;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith({SpringExtension.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = Launcher.class)
public abstract class JavaFXIntegrationTest {
    static final CountDownLatch initialized = new CountDownLatch(1);
    static final AtomicBoolean startedInitializing = new AtomicBoolean(false);

    @BeforeAll
    static void initJfxRuntime() throws InterruptedException {
        if (startedInitializing.getAndSet(true)) {
            System.out.println("[FXTest] Waiting for JavaFX toolkit initialization latch...");
            initialized.await();
            return;
        }

        System.out.println("[FXTest] Initializing JavaFX toolkit...");
        Platform.startup(
                () -> {
                    System.out.println("[FXTest] JavaFX toolkit initialized.");
                    initialized.countDown();
                });

        initialized.await();
    }

    @AfterAll
    static void afterAll() {
        Platform.exit();
    }
}
