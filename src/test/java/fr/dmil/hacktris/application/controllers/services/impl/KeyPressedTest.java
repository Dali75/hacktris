package fr.dmil.hacktris.application.controllers.services.impl;

import static fr.dmil.hacktris.application.controllers.services.impl.KeyPressed.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import fr.dmil.hacktris.JavaFXIntegrationTest;
import fr.dmil.hacktris.domain.hacktris.services.HacktrisService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

class KeyPressedTest extends JavaFXIntegrationTest {
    @MockBean private HacktrisService hacktrisService;

    @Test
    void should_rotate() {
        // Data
        UP.hacktrisService = hacktrisService;

        // Call
        UP.processAction();

        // Verify
        verify(hacktrisService, times(1)).rotate();
    }

    @Test
    void should_left() {
        // Data
        LEFT.hacktrisService = hacktrisService;

        // Call
        LEFT.processAction();

        // Verify
        verify(hacktrisService, times(1)).left();
    }

    @Test
    void should_right() {
        // Data
        RIGHT.hacktrisService = hacktrisService;

        // Call
        RIGHT.processAction();

        // Verify
        verify(hacktrisService, times(1)).right();
    }

    @Test
    void should_down() {
        // Data
        DOWN.hacktrisService = hacktrisService;

        // Call
        DOWN.processAction();

        // Verify
        verify(hacktrisService, times(1)).down();
    }

    @Test
    void should_hardDrop() {
        // Data
        SPACE.hacktrisService = hacktrisService;

        // Call
        SPACE.processAction();

        // Verify
        verify(hacktrisService, times(1)).hardDrop();
    }

    @Test
    void should_hold() {
        // Data
        H.hacktrisService = hacktrisService;

        // Call
        H.processAction();

        // Verify
        verify(hacktrisService, times(1)).hold();
    }

    @Test
    void should_PauseOrResume() {
        // Data
        P.hacktrisService = hacktrisService;

        // Call
        P.processAction();

        // Verify
        verify(hacktrisService, times(1)).pauseOrResume();
    }
}
