package fr.dmil.hacktris.application.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import fr.dmil.hacktris.JavaFXIntegrationTest;
import fr.dmil.hacktris.application.controllers.services.HacktrisControllerService;
import javafx.scene.input.KeyEvent;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

class HacktrisControllerTest extends JavaFXIntegrationTest {
    @Autowired private HacktrisController hacktrisController;
    @MockBean private HacktrisControllerService hacktrisControllerService;

    @Test
    void should_start() {
        // Call
        hacktrisController.start();

        // Verify
        verify(hacktrisControllerService, times(1)).start();
    }

    @Test
    void should_stop() {
        // Call
        hacktrisController.stop();

        // Verify
        verify(hacktrisControllerService, times(1)).stop();
    }

    @Test
    void should_handle() {
        // Data
        var keyEvent = Mockito.mock(KeyEvent.class);

        // Call
        hacktrisController.handle(keyEvent);

        // Verify
        verify(hacktrisControllerService, times(1)).handle(keyEvent);
    }
}
