package fr.dmil.hacktris.application.common.services;

import static fr.dmil.hacktris.application.common.services.impl.I18nResourceBundleServiceImpl.MESSAGES;
import static java.util.Locale.*;
import static java.util.ResourceBundle.getBundle;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import fr.dmil.hacktris.application.common.services.impl.I18nResourceBundleServiceImpl;
import java.util.MissingResourceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class I18nResourceBundleServiceTest {
    private final I18nResourceBundleService i18nResourceBundleService =
            new I18nResourceBundleServiceImpl();

    @BeforeEach
    void setUp() {
        var bundle = getBundle(MESSAGES, FRENCH);
        setField(i18nResourceBundleService, "resourceBundle", bundle);
    }

    @Test
    void getMsgShouldReturnAFrenchValue() {
        assertThat(i18nResourceBundleService.getMsg("game")).isEqualTo("Jeu");
    }

    @Test
    void getMsgShouldThrowException() {
        assertThatThrownBy(() -> i18nResourceBundleService.getMsg("gamez"))
                .isInstanceOf(MissingResourceException.class)
                .hasMessage(
                        "Can't find resource for bundle java.util.PropertyResourceBundle, key gamez");
    }
}
