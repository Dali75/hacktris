package fr.dmil.hacktris.architecture;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import org.springframework.stereotype.Component;

@AnalyzeClasses(packages = {"fr.dmil.hacktris"})
class ArchitectureTest {
    public static final String ROOT_PACKAGE = "fr.dmil.hacktris.%s..";

    @ArchTest
    void checkDependencies(JavaClasses javaClasses) {
        checkNoDependencyFromTo(
                javaClasses, "domain", "application", "infrastructure", "configuration");
        checkNoDependencyFromTo(javaClasses, "infrastructure", "application", "configuration");
        checkNoDependencyFromTo(javaClasses, "application", "infrastructure", "configuration");
    }

    @ArchTest
    void checkServicesClassesHavingNameEndingWithService(JavaClasses javaClasses) {
        classes()
                .that()
                .resideInAPackage("..services")
                .and()
                .haveSimpleNameNotEndingWith("Test")
                .should()
                .haveSimpleNameEndingWith("Service")
                .check(javaClasses);
    }

    @ArchTest
    void checkServicesImplClassesImplementingServicesHavingNameEndingWithServiceImpl(
            JavaClasses javaClasses) {

        classes()
                .that()
                .resideInAPackage("..services.impl")
                .and()
                .haveSimpleNameEndingWith("ServiceImpl")
                .should(implementServiceInterfaceCondition())
                .check(javaClasses);
    }

    @ArchTest
    void checkEventsClassesHavingNameEndingWithEvent(JavaClasses javaClasses) {
        classes()
                .that()
                .resideInAPackage("..events")
                .and()
                .haveSimpleNameNotEndingWith("Test")
                .and()
                .areNotAnnotatedWith(Component.class)
                .should()
                .haveSimpleNameEndingWith("Event")
                .check(javaClasses);
    }

    private void checkNoDependencyFromTo(
            JavaClasses classesToCheck, String fromPackage, String... toPackages) {
        noClasses()
                .that()
                .resideInAPackage(ROOT_PACKAGE.formatted(fromPackage))
                .should()
                .dependOnClassesThat()
                .resideInAnyPackage(toPackages)
                .check(classesToCheck);
    }

    private ArchCondition<JavaClass> implementServiceInterfaceCondition() {
        return new ArchCondition<>("implement an interface ending with 'Service' in '..services'") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                boolean notImplementedCorrectInterface =
                        item.getRawInterfaces().stream()
                                .noneMatch(
                                        i ->
                                                i.getPackage().containsPackage(".services")
                                                        && i.getSimpleName().endsWith("Service"));

                var message =
                        String.format(
                                "%s does not implement an interface ending with 'Service' in '..services'",
                                item.getSimpleName());
                events.add(new SimpleConditionEvent(item, notImplementedCorrectInterface, message));
            }
        };
    }
}
