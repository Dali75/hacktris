package fr.dmil.hacktris.domain.events.services;

public interface PublisherService {
    void publishEvent(Object event);
}
