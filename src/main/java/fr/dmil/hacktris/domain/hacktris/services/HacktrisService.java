package fr.dmil.hacktris.domain.hacktris.services;

import fr.dmil.hacktris.domain.hacktris.enums.HacktrisStatus;

public interface HacktrisService {

    void start();

    void stop();

    boolean isGameRunning();

    boolean isGamePaused();

    boolean shouldPlayFallSound(HacktrisStatus status);

    HacktrisStatus down();

    void pauseOrResume();

    void hold();

    HacktrisStatus hardDrop();

    void rotate();

    void left();

    void right();
}
