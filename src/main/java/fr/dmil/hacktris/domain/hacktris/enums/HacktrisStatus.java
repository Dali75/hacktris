package fr.dmil.hacktris.domain.hacktris.enums;

public enum HacktrisStatus {
    GAME_ALIVE,
    LINES_MADE,
    GAME_OVER,
    NEW_HACKTRIMINO
}
