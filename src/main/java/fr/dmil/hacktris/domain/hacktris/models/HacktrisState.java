package fr.dmil.hacktris.domain.hacktris.models;

import fr.dmil.hacktris.domain.hacktriminos.models.Hacktrimino;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HacktrisState {
    private boolean isGameRunning;
    private boolean isGamePaused;
    private Hacktrimino currentHacktrimino;
    private Hacktrimino nextHacktrimino;
    private Hacktrimino hacktriminoHeld;
    private boolean canHoldHacktrimino;
}
