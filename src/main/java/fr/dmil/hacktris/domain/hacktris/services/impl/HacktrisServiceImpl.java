package fr.dmil.hacktris.domain.hacktris.services.impl;

import static fr.dmil.hacktris.domain.hacktris.enums.HacktrisStatus.*;

import fr.dmil.hacktris.domain.grids.auxiliary.services.HacktrisAuxiliaryGridService;
import fr.dmil.hacktris.domain.grids.main.services.HacktrisMainGridService;
import fr.dmil.hacktris.domain.hacktriminos.models.Hacktrimino;
import fr.dmil.hacktris.domain.hacktriminos.services.HacktriminoService;
import fr.dmil.hacktris.domain.hacktris.enums.HacktrisStatus;
import fr.dmil.hacktris.domain.hacktris.models.HacktrisState;
import fr.dmil.hacktris.domain.hacktris.services.HacktrisService;
import fr.dmil.hacktris.domain.properties.services.PropertiesService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class HacktrisServiceImpl implements HacktrisService {
    private final HacktrisState hacktrisState = new HacktrisState();
    private final PropertiesService propertiesService;
    private final HacktriminoService hacktriminoService;
    private final HacktrisMainGridService hacktrisMainGridService;
    private final HacktrisAuxiliaryGridService hacktrisHoldGridService;
    private final HacktrisAuxiliaryGridService hacktrisNextGridService;

    @Override
    public void start() {
        hacktrisState.setGamePaused(false);
        hacktrisState.setGameRunning(true);
        hacktrisState.setCurrentHacktrimino(hacktriminoService.generate());
        hacktrisState.setNextHacktrimino(hacktriminoService.generate());
        hacktrisState.setHacktriminoHeld(null);
        hacktrisState.setCanHoldHacktrimino(true);
        hacktrisNextGridService.add(hacktrisState.getNextHacktrimino());
    }

    @Override
    public void stop() {
        hacktrisState.setGameRunning(false);
        hacktrisState.setGamePaused(false);
        hacktrisMainGridService.reset();
        hacktrisHoldGridService.reset();
        hacktrisNextGridService.reset();
    }

    @Override
    public boolean isGameRunning() {
        return hacktrisState.isGameRunning();
    }

    @Override
    public boolean isGamePaused() {
        return hacktrisState.isGamePaused();
    }

    @Override
    public boolean shouldPlayFallSound(HacktrisStatus status) {
        return List.of(NEW_HACKTRIMINO, LINES_MADE).contains(status);
    }

    @Override
    public void pauseOrResume() {
        hacktrisState.setGamePaused(!hacktrisState.isGamePaused());
    }

    @Override
    public void hold() {
        var hacktriminoToHold = hacktrisState.getCurrentHacktrimino();
        if (hacktrisState.isCanHoldHacktrimino()) {
            hacktrisMainGridService.remove(hacktriminoToHold);
            hacktrisHoldGridService.add(hacktriminoToHold);

            var hacktriminoOptional = findHacktriminoHeld();
            if (hacktriminoOptional.isEmpty()) {
                hacktrisState.setCurrentHacktrimino(hacktrisState.getNextHacktrimino());
                hacktrisState.setNextHacktrimino(hacktriminoService.generate());
                hacktrisNextGridService.add(hacktrisState.getNextHacktrimino());
            } else {
                hacktrisState.setCurrentHacktrimino(hacktriminoOptional.get());
            }
            var hacktriminoHeld = hacktriminoService.generateHeldForHacktrisGrid(hacktriminoToHold);
            hacktrisState.setHacktriminoHeld(hacktriminoHeld);
            hacktrisState.setCanHoldHacktrimino(false);
        }
    }

    @Override
    public HacktrisStatus down() {
        return doDown();
    }

    @Override
    public HacktrisStatus hardDrop() {
        HacktrisStatus hacktrisStatus;
        do {
            hacktrisStatus = doDown();
        } while (hacktrisStatus == GAME_ALIVE);
        return hacktrisStatus;
    }

    @Override
    public void rotate() {
        hacktrisMainGridService.rotate(hacktrisState.getCurrentHacktrimino());
    }

    @Override
    public void left() {
        hacktrisMainGridService.goLeft(hacktrisState.getCurrentHacktrimino());
    }

    @Override
    public void right() {
        hacktrisMainGridService.goRight(hacktrisState.getCurrentHacktrimino());
    }

    private HacktrisStatus doDown() {
        var currentHacktrimino = hacktrisState.getCurrentHacktrimino();
        var result = hacktrisMainGridService.goDown(currentHacktrimino);

        if (result) {
            return GAME_ALIVE;
        }
        if (currentHacktrimino.isOutOfBounds(propertiesService.startingLine())) {
            return GAME_OVER;
        }
        var status = NEW_HACKTRIMINO;
        var linesMade = hacktrisMainGridService.clearLinesMade();
        if (linesMade > 0) {
            status = LINES_MADE;
        }
        hacktrisState.setCurrentHacktrimino(hacktrisState.getNextHacktrimino());
        hacktrisState.setNextHacktrimino(hacktriminoService.generate());
        hacktrisState.setCanHoldHacktrimino(true);
        hacktrisNextGridService.add(hacktrisState.getNextHacktrimino());
        return status;
    }

    private Optional<Hacktrimino> findHacktriminoHeld() {
        return Optional.ofNullable(hacktrisState.getHacktriminoHeld());
    }
}
