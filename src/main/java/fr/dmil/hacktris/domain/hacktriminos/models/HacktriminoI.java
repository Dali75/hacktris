package fr.dmil.hacktris.domain.hacktriminos.models;

import fr.dmil.hacktris.domain.grids.models.HacktriminoCell;
import fr.dmil.hacktris.domain.grids.models.HacktriminoICell;
import fr.dmil.hacktris.domain.points.models.Point;
import lombok.ToString;

@ToString(callSuper = true)
public class HacktriminoI extends AbstractHacktrimino {

    public HacktriminoI(int x, int y) {
        super(x - 1, y);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() + 1, getY()));
        points.add(new Point(getX() + 2, getY()));
        points.add(new Point(getX() + 3, getY()));
    }

    private HacktriminoI(int x, int y, Rotation rotation) {
        super(x, y);
        setRotation(rotation);
    }

    @Override
    public void firstRotation() {
        setXY(2, -1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX(), getY() + 1));
        points.add(new Point(getX(), getY() + 2));
        points.add(new Point(getX(), getY() + 3));
    }

    @Override
    public void secondRotation() {
        setXY(1, 2);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() - 1, getY()));
        points.add(new Point(getX() - 2, getY()));
        points.add(new Point(getX() - 3, getY()));
    }

    @Override
    public void thirdRotation() {
        setXY(-2, 1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX(), getY() - 1));
        points.add(new Point(getX(), getY() - 2));
        points.add(new Point(getX(), getY() - 3));
    }

    @Override
    public void fourthRotation() {
        setXY(-1, -2);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() + 1, getY()));
        points.add(new Point(getX() + 2, getY()));
        points.add(new Point(getX() + 3, getY()));
    }

    @Override
    public HacktriminoCell getComposingCell() {
        return new HacktriminoICell(getId());
    }

    @Override
    public Hacktrimino copy() {
        return new HacktriminoI(getX(), getY(), getRotation());
    }
}
