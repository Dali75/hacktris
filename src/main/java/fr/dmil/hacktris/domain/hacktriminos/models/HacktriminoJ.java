package fr.dmil.hacktris.domain.hacktriminos.models;

import fr.dmil.hacktris.domain.grids.models.HacktriminoCell;
import fr.dmil.hacktris.domain.grids.models.HacktriminoJCell;
import fr.dmil.hacktris.domain.points.models.Point;
import lombok.ToString;

@ToString(callSuper = true)
public class HacktriminoJ extends AbstractHacktrimino {
    public HacktriminoJ(int x, int y) {
        super(x - 1, y + 1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX(), getY() - 1));
        points.add(new Point(getX() + 1, getY()));
        points.add(new Point(getX() + 2, getY()));
    }

    private HacktriminoJ(int x, int y, Rotation rotation) {
        super(x, y);
        setRotation(rotation);
    }

    @Override
    public void firstRotation() {
        setXY(1, -1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() + 1, getY()));
        points.add(new Point(getX(), getY() + 1));
        points.add(new Point(getX(), getY() + 2));
    }

    @Override
    public void secondRotation() {
        setXY(1, 1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX(), getY() + 1));
        points.add(new Point(getX() - 1, getY()));
        points.add(new Point(getX() - 2, getY()));
    }

    @Override
    public void thirdRotation() {
        setXY(-1, 1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() - 1, getY()));
        points.add(new Point(getX(), getY() - 1));
        points.add(new Point(getX(), getY() - 2));
    }

    @Override
    public void fourthRotation() {
        setXY(-1, -1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX(), getY() - 1));
        points.add(new Point(getX() + 1, getY()));
        points.add(new Point(getX() + 2, getY()));
    }

    @Override
    public HacktriminoCell getComposingCell() {
        return new HacktriminoJCell(getId());
    }

    @Override
    public Hacktrimino copy() {
        return new HacktriminoJ(getX(), getY(), getRotation());
    }
}
