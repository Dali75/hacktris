package fr.dmil.hacktris.domain.hacktriminos.models;

import static fr.dmil.hacktris.domain.hacktriminos.models.Rotation.FIRST_ROTATION;

import fr.dmil.hacktris.domain.points.models.Point;
import java.util.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@ToString
public abstract class AbstractHacktrimino implements Hacktrimino {
    private UUID id;
    private int x;
    private int y;
    private Rotation rotation;
    protected List<Point> points = new ArrayList<>();

    protected AbstractHacktrimino(int x, int y) {
        this.x = x;
        this.y = y;
        this.id = UUID.randomUUID();
    }

    protected void setXY(int xValue, int yValue) {
        x += xValue;
        y += yValue;
    }

    @Override
    public List<Point> getPoints() {
        return points;
    }

    @Override
    public boolean isOutOfBounds(int startingLine) {
        return y == startingLine;
    }

    @Override
    public void goLeft() {
        x--;
        for (Point point : points) {
            point.setX(point.getX() - 1);
        }
    }

    @Override
    public void goRight() {
        x++;
        for (Point point : points) {
            point.setX(point.getX() + 1);
        }
    }

    @Override
    public void goDown() {
        y++;
        for (Point point : points) {
            point.setY(point.getY() + 1);
        }
    }

    @Override
    public void goUp() {
        y--;
        for (Point point : points) {
            point.setY(point.getY() - 1);
        }
    }

    @Override
    public void rotateRight() {
        rotation = Optional.ofNullable(rotation).map(Rotation::next).orElse(FIRST_ROTATION);
        rotation.execute(this);
    }
}
