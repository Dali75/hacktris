package fr.dmil.hacktris.domain.hacktriminos.models;

import fr.dmil.hacktris.domain.grids.models.HacktriminoCell;
import fr.dmil.hacktris.domain.grids.models.HacktriminoOCell;
import fr.dmil.hacktris.domain.points.models.Point;
import lombok.ToString;

@ToString(callSuper = true)
public class HacktriminoO extends AbstractHacktrimino {
    public HacktriminoO(int x, int y) {
        super(x, y);
        setPointsAsSquareOfFourPoints();
    }

    private HacktriminoO(int x, int y, Rotation rotation) {
        super(x, y);
        setRotation(rotation);
    }

    @Override
    public void firstRotation() {
        setPointsAsSquareOfFourPoints();
    }

    @Override
    public void secondRotation() {
        setPointsAsSquareOfFourPoints();
    }

    @Override
    public void thirdRotation() {
        setPointsAsSquareOfFourPoints();
    }

    @Override
    public void fourthRotation() {
        setPointsAsSquareOfFourPoints();
    }

    @Override
    public HacktriminoCell getComposingCell() {
        return new HacktriminoOCell(getId());
    }

    @Override
    public Hacktrimino copy() {
        return new HacktriminoO(getX(), getY(), getRotation());
    }

    private void setPointsAsSquareOfFourPoints() {
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() + 1, getY()));
        points.add(new Point(getX(), getY() + 1));
        points.add(new Point(getX() + 1, getY() + 1));
    }
}
