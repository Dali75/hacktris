package fr.dmil.hacktris.domain.hacktriminos.models;

import fr.dmil.hacktris.domain.grids.models.HacktriminoCell;
import fr.dmil.hacktris.domain.points.models.Point;
import java.util.List;

public interface Hacktrimino {

    List<Point> getPoints();

    boolean isOutOfBounds(int startingLine);

    void goLeft();

    void goRight();

    void goDown();

    void goUp();

    void rotateRight();

    HacktriminoCell getComposingCell();

    Hacktrimino copy();

    void firstRotation();

    void secondRotation();

    void thirdRotation();

    void fourthRotation();
}
