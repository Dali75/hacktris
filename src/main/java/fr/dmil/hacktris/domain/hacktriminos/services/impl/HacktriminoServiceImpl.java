package fr.dmil.hacktris.domain.hacktriminos.services.impl;

import fr.dmil.hacktris.domain.hacktriminos.models.*;
import fr.dmil.hacktris.domain.hacktriminos.services.HacktriminoService;
import fr.dmil.hacktris.domain.properties.services.PropertiesService;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HacktriminoServiceImpl implements HacktriminoService {
    private final Random random;
    private final int startingLine;
    private final int holdGridMiddleColumn;
    private final int hacktrisGridMiddleColumn;
    private final int nextGridMiddleColumn;
    private final HacktriminoFactory hacktriminoFactory;
    private final List<Class<? extends Hacktrimino>> hacktriminoClasses;

    public HacktriminoServiceImpl(
            PropertiesService propertiesService, HacktriminoFactory hacktriminoFactory) {
        this.hacktriminoClasses = new ArrayList<>();
        this.hacktriminoClasses.add(HacktriminoO.class);
        this.hacktriminoClasses.add(HacktriminoI.class);
        this.hacktriminoClasses.add(HacktriminoT.class);
        this.hacktriminoClasses.add(HacktriminoL.class);
        this.hacktriminoClasses.add(HacktriminoJ.class);
        this.hacktriminoClasses.add(HacktriminoZ.class);
        this.hacktriminoClasses.add(HacktriminoS.class);
        this.random = new SecureRandom();
        this.hacktriminoFactory = hacktriminoFactory;
        this.hacktrisGridMiddleColumn = (propertiesService.gridMaxColumns() / 2) - 1;
        this.holdGridMiddleColumn = (propertiesService.holdGridMaxColumns() / 2) - 1;
        this.nextGridMiddleColumn = (propertiesService.nextGridMaxColumns() / 2) - 1;
        this.startingLine = propertiesService.startingLine();
    }

    @Override
    public Hacktrimino generate() {
        int randomNumber = random.nextInt(0, hacktriminoClasses.size());
        var hacktrimino = generate(hacktrisGridMiddleColumn, startingLine, randomNumber);
        log.info("{} generated", hacktrimino);
        return hacktrimino;
    }

    @Override
    public Hacktrimino generateHeld(Hacktrimino hacktrimino) {
        return hacktriminoFactory.create(hacktrimino.getClass(), holdGridMiddleColumn, 0);
    }

    @Override
    public Hacktrimino generateNext(Hacktrimino hacktrimino) {
        return hacktriminoFactory.create(hacktrimino.getClass(), nextGridMiddleColumn, 0);
    }

    @Override
    public Hacktrimino generateHeldForHacktrisGrid(Hacktrimino hacktrimino) {
        var aClass = hacktrimino.getClass();
        return hacktriminoFactory.create(aClass, hacktrisGridMiddleColumn, startingLine);
    }

    private Hacktrimino generate(int x, int y, int randomNumber) {
        return hacktriminoFactory.create(hacktriminoClasses.get(randomNumber), x, y);
    }
}
