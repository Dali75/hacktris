package fr.dmil.hacktris.domain.hacktriminos.services;

import fr.dmil.hacktris.domain.hacktriminos.models.*;

public interface HacktriminoService {
    Hacktrimino generate();

    Hacktrimino generateHeldForHacktrisGrid(Hacktrimino hacktrimino);

    Hacktrimino generateHeld(Hacktrimino hacktrimino);

    Hacktrimino generateNext(Hacktrimino hacktrimino);
}
