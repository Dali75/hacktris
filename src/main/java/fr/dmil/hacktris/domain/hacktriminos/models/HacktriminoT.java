package fr.dmil.hacktris.domain.hacktriminos.models;

import fr.dmil.hacktris.domain.grids.models.HacktriminoCell;
import fr.dmil.hacktris.domain.grids.models.HacktriminoTCell;
import fr.dmil.hacktris.domain.points.models.Point;
import lombok.ToString;

@ToString(callSuper = true)
public class HacktriminoT extends AbstractHacktrimino {
    public HacktriminoT(int x, int y) {
        super(x - 1, y + 1);
        addPointsAsReversedHorizontalT();
    }

    private HacktriminoT(int x, int y, Rotation rotation) {
        super(x, y);
        setRotation(rotation);
    }

    @Override
    public void firstRotation() {
        setXY(1, -1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX(), getY() + 1));
        points.add(new Point(getX(), getY() + 2));
        points.add(new Point(getX() + 1, getY() + 1));
    }

    @Override
    public void secondRotation() {
        setXY(1, 1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() - 1, getY()));
        points.add(new Point(getX() - 2, getY()));
        points.add(new Point(getX() - 1, getY() + 1));
    }

    @Override
    public void thirdRotation() {
        setXY(-1, 1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX(), getY() - 1));
        points.add(new Point(getX(), getY() - 2));
        points.add(new Point(getX() - 1, getY() - 1));
    }

    @Override
    public void fourthRotation() {
        setXY(-1, -1);
        addPointsAsReversedHorizontalT();
    }

    @Override
    public HacktriminoCell getComposingCell() {
        return new HacktriminoTCell(getId());
    }

    @Override
    public Hacktrimino copy() {
        return new HacktriminoT(getX(), getY(), getRotation());
    }

    private void addPointsAsReversedHorizontalT() {
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() + 1, getY()));
        points.add(new Point(getX() + 2, getY()));
        points.add(new Point(getX() + 1, getY() - 1));
    }
}
