package fr.dmil.hacktris.domain.hacktriminos.services.impl;

public class HacktriminoServiceException extends RuntimeException {
    public HacktriminoServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
