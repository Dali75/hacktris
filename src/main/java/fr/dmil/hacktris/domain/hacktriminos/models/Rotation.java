package fr.dmil.hacktris.domain.hacktriminos.models;

public enum Rotation {
    FIRST_ROTATION {
        @Override
        Rotation next() {
            return SECOND_ROTATION;
        }

        @Override
        void execute(Hacktrimino hacktrimino) {
            hacktrimino.firstRotation();
        }
    },
    SECOND_ROTATION {
        @Override
        Rotation next() {
            return THIRD_ROTATION;
        }

        @Override
        void execute(Hacktrimino hacktrimino) {
            hacktrimino.secondRotation();
        }
    },
    THIRD_ROTATION {
        @Override
        Rotation next() {
            return FOURTH_ROTATION;
        }

        @Override
        void execute(Hacktrimino hacktrimino) {
            hacktrimino.thirdRotation();
        }
    },
    FOURTH_ROTATION {
        @Override
        Rotation next() {
            return FIRST_ROTATION;
        }

        @Override
        void execute(Hacktrimino hacktrimino) {
            hacktrimino.fourthRotation();
        }
    };

    abstract Rotation next();

    abstract void execute(Hacktrimino hacktrimino);
}
