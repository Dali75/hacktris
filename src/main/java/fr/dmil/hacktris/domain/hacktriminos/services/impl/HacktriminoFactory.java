package fr.dmil.hacktris.domain.hacktriminos.services.impl;

import fr.dmil.hacktris.domain.hacktriminos.models.Hacktrimino;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class HacktriminoFactory {
    private static final String ERROR_MSG =
            "An error occured during the generation of a hacktrimino of type %s";

    public Hacktrimino create(Class<? extends Hacktrimino> clazz, int x, int y) {
        try {
            Constructor<? extends Hacktrimino> declaredConstructor =
                    clazz.getDeclaredConstructor(int.class, int.class);
            return declaredConstructor.newInstance(x, y);

        } catch (NoSuchMethodException
                | InvocationTargetException
                | InstantiationException
                | IllegalAccessException e) {
            throw new HacktriminoServiceException(ERROR_MSG.formatted(clazz.getSimpleName()), e);
        }
    }
}
