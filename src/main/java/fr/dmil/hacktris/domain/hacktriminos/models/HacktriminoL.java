package fr.dmil.hacktris.domain.hacktriminos.models;

import fr.dmil.hacktris.domain.grids.models.HacktriminoCell;
import fr.dmil.hacktris.domain.grids.models.HacktriminoLCell;
import fr.dmil.hacktris.domain.points.models.Point;
import lombok.ToString;

@ToString(callSuper = true)
public class HacktriminoL extends AbstractHacktrimino {
    public HacktriminoL(int x, int y) {
        super(x - 1, y + 1);
        addPointsAsHorizontalL();
    }

    private HacktriminoL(int x, int y, Rotation rotation) {
        super(x, y);
        setRotation(rotation);
    }

    @Override
    public void firstRotation() {
        setXY(1, -1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX(), getY() + 1));
        points.add(new Point(getX(), getY() + 2));
        points.add(new Point(getX() + 1, getY() + 2));
    }

    @Override
    public void secondRotation() {
        setXY(1, 1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() - 1, getY()));
        points.add(new Point(getX() - 2, getY()));
        points.add(new Point(getX() - 2, getY() + 1));
    }

    @Override
    public void thirdRotation() {
        setXY(-1, 1);
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX(), getY() - 1));
        points.add(new Point(getX(), getY() - 2));
        points.add(new Point(getX() - 1, getY() - 2));
    }

    @Override
    public void fourthRotation() {
        setXY(-1, -1);
        addPointsAsHorizontalL();
    }

    @Override
    public HacktriminoCell getComposingCell() {
        return new HacktriminoLCell(getId());
    }

    @Override
    public Hacktrimino copy() {
        return new HacktriminoL(getX(), getY(), getRotation());
    }

    private void addPointsAsHorizontalL() {
        points.clear();
        points.add(new Point(getX(), getY()));
        points.add(new Point(getX() + 1, getY()));
        points.add(new Point(getX() + 2, getY()));
        points.add(new Point(getX() + 2, getY() - 1));
    }
}
