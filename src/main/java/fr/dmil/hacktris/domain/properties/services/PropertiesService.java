package fr.dmil.hacktris.domain.properties.services;

public interface PropertiesService {
    int startingLine();

    int gridMaxLines();

    int gridMaxColumns();

    int holdGridMaxLines();

    int holdGridMaxColumns();

    int nextGridMaxLines();

    int nextGridMaxColumns();
}
