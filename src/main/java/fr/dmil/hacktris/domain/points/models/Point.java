package fr.dmil.hacktris.domain.points.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Point {
    private int x;
    private int y;
}
