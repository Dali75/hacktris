package fr.dmil.hacktris.domain.grids.auxiliary.services;

import fr.dmil.hacktris.domain.hacktriminos.models.Hacktrimino;

public interface HacktrisAuxiliaryGridService {
    void add(Hacktrimino hacktrimino);

    void reset();
}
