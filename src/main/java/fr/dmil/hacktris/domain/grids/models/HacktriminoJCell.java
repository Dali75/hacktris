package fr.dmil.hacktris.domain.grids.models;

import java.util.UUID;

public class HacktriminoJCell extends HacktriminoCell {

    public HacktriminoJCell(UUID id) {
        super(id, "J");
    }
}
