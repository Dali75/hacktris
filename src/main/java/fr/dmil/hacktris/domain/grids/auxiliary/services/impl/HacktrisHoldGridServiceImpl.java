package fr.dmil.hacktris.domain.grids.auxiliary.services.impl;

import fr.dmil.hacktris.domain.events.services.PublisherService;
import fr.dmil.hacktris.domain.grids.auxiliary.services.HacktrisAuxiliaryGridService;
import fr.dmil.hacktris.domain.grids.events.HacktrisGridChangedEvent;
import fr.dmil.hacktris.domain.grids.services.AbstractGridService;
import fr.dmil.hacktris.domain.hacktriminos.models.Hacktrimino;
import fr.dmil.hacktris.domain.hacktriminos.services.HacktriminoService;
import fr.dmil.hacktris.domain.properties.services.PropertiesService;

public class HacktrisHoldGridServiceImpl extends AbstractGridService
        implements HacktrisAuxiliaryGridService {

    private final HacktriminoService hacktriminoService;

    public HacktrisHoldGridServiceImpl(
            PublisherService publisherService,
            PropertiesService propertiesService,
            HacktriminoService hacktriminoService,
            HacktrisGridChangedEvent hacktrisGridChangedEvent) {
        super(
                propertiesService.holdGridMaxLines(),
                propertiesService.holdGridMaxColumns(),
                publisherService,
                hacktrisGridChangedEvent,
                null);
        this.hacktriminoService = hacktriminoService;
    }

    @Override
    public void add(Hacktrimino hacktrimino) {
        doReset();
        super.add(hacktriminoService.generateHeld(hacktrimino));
    }
}
