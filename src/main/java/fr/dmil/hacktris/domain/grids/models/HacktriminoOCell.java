package fr.dmil.hacktris.domain.grids.models;

import java.util.UUID;

public class HacktriminoOCell extends HacktriminoCell {

    public HacktriminoOCell(UUID id) {
        super(id, "O");
    }
}
