package fr.dmil.hacktris.domain.grids.models;

import java.util.UUID;

public class HacktriminoEmptyCell extends HacktriminoCell {
    public HacktriminoEmptyCell(UUID id) {
        super(id, "");
    }
}
