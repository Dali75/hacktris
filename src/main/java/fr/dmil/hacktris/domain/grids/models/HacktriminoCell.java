package fr.dmil.hacktris.domain.grids.models;

import java.util.UUID;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class HacktriminoCell {
    private final UUID id;
    private final String code;

    public HacktriminoCell(UUID id, String code) {
        this.id = id;
        this.code = code;
    }

    public boolean isEmptyCell() {
        return "".equals(code);
    }
}
