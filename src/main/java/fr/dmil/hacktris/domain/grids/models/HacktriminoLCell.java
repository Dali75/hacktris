package fr.dmil.hacktris.domain.grids.models;

import java.util.UUID;

public class HacktriminoLCell extends HacktriminoCell {

    public HacktriminoLCell(UUID id) {
        super(id, "L");
    }
}
