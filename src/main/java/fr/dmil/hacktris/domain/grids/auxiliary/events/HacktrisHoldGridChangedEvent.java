package fr.dmil.hacktris.domain.grids.auxiliary.events;

import fr.dmil.hacktris.domain.grids.events.HacktrisGridChangedEvent;

public interface HacktrisHoldGridChangedEvent extends HacktrisGridChangedEvent {}
