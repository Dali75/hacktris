package fr.dmil.hacktris.domain.grids.models;

import java.util.UUID;

public class HacktriminoTCell extends HacktriminoCell {

    public HacktriminoTCell(UUID id) {
        super(id, "T");
    }
}
