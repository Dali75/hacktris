package fr.dmil.hacktris.domain.grids.models;

import lombok.Getter;

@Getter
public class HacktrisGrid {
    private final int maxLines;
    private final int maxColumns;
    private final HacktriminoCell[][] grid;

    public HacktrisGrid(int maxLines, int maxColumns) {
        this.grid = new HacktriminoCell[maxLines][maxColumns];
        this.maxLines = maxLines;
        this.maxColumns = maxColumns;
    }

    public HacktriminoCell getCell(int y, int x) {
        return grid[y][x];
    }

    public void setCell(int y, int x, HacktriminoCell cell) {
        if (doAreInBounds(y, x)) {
            grid[y][x] = cell;
        }
    }

    public void setEmptyCell(int y, int x) {
        if (doAreInBounds(y, x)) {
            grid[y][x] = new HacktriminoEmptyCell(null);
        }
    }

    public boolean isEmptyCell(int y, int x) {
        return doAreInBounds(y, x) && grid[y][x].isEmptyCell();
    }

    public boolean areInBounds(int y, int x) {
        return doAreInBounds(y, x);
    }

    private boolean doAreInBounds(int y, int x) {
        return x >= 0 && x < maxColumns && y >= 0 && y < maxLines;
    }

    public boolean isEmptyCell(int x, int y, HacktriminoCell cell) {
        return doAreInBounds(y, x)
                && (grid[y][x].isEmptyCell() || grid[y][x].getId() == cell.getId());
    }
}
