package fr.dmil.hacktris.domain.grids.models;

import java.util.UUID;

public class HacktriminoZCell extends HacktriminoCell {

    public HacktriminoZCell(UUID id) {
        super(id, "Z");
    }
}
