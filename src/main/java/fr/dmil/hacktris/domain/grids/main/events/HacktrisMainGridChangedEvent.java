package fr.dmil.hacktris.domain.grids.main.events;

import fr.dmil.hacktris.domain.grids.events.HacktrisGridChangedEvent;

public interface HacktrisMainGridChangedEvent extends HacktrisGridChangedEvent {}
