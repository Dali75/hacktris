package fr.dmil.hacktris.domain.grids.events;

import fr.dmil.hacktris.domain.grids.models.HacktrisGrid;

public interface HacktrisGridChangedEvent {
    void setGrid(HacktrisGrid grid);
}
