package fr.dmil.hacktris.domain.grids.models;

import java.util.UUID;

public class HacktriminoSCell extends HacktriminoCell {

    public HacktriminoSCell(UUID id) {
        super(id, "S");
    }
}
