package fr.dmil.hacktris.domain.grids.main.services.impl;

import fr.dmil.hacktris.domain.events.services.PublisherService;
import fr.dmil.hacktris.domain.grids.events.HacktrisGridChangedEvent;
import fr.dmil.hacktris.domain.grids.main.events.LinesClearedEvent;
import fr.dmil.hacktris.domain.grids.main.services.HacktrisMainGridService;
import fr.dmil.hacktris.domain.grids.models.HacktriminoEmptyCell;
import fr.dmil.hacktris.domain.grids.services.AbstractGridService;
import fr.dmil.hacktris.domain.hacktriminos.models.Hacktrimino;
import fr.dmil.hacktris.domain.points.models.Point;
import fr.dmil.hacktris.domain.properties.services.PropertiesService;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HacktrisMainGridServiceImpl extends AbstractGridService
        implements HacktrisMainGridService {
    private final PropertiesService propertiesService;
    private final LinesClearedEvent linesClearedEvent;

    public HacktrisMainGridServiceImpl(
            PublisherService publisherService,
            PropertiesService propertiesService,
            LinesClearedEvent linesClearedEvent,
            HacktrisGridChangedEvent hacktrisGridChangedEvent) {
        super(
                propertiesService.gridMaxLines(),
                propertiesService.gridMaxColumns(),
                publisherService,
                hacktrisGridChangedEvent,
                new HacktriminoEmptyCell(null));
        this.propertiesService = propertiesService;
        this.linesClearedEvent = linesClearedEvent;
    }

    @Override
    public int clearLinesMade() {
        int gridMaxLines = propertiesService.gridMaxLines();
        int linesCleared = 0;
        for (int i = 0; i < gridMaxLines; i++) {
            if (isLineCleared(i)) {
                moveLinesToLine(i);
                linesCleared++;
            }
        }

        refreshGrid();
        refreshLinesClearedCounter(linesCleared);
        return linesCleared;
    }

    @Override
    public void rotate(Hacktrimino hacktrimino) {
        removeOnly(hacktrimino);
        if (canRotate(hacktrimino)) {
            hacktrimino.rotateRight();
        }
        add(hacktrimino);
    }

    @Override
    public void goLeft(Hacktrimino hacktrimino) {
        removeOnly(hacktrimino);
        doGoLeft(hacktrimino);
        add(hacktrimino);
    }

    @Override
    public void goRight(Hacktrimino hacktrimino) {
        removeOnly(hacktrimino);
        removeOnly(hacktrimino);
        doGoRight(hacktrimino);
        add(hacktrimino);
    }

    @Override
    public boolean goDown(Hacktrimino hacktrimino) {
        removeOnly(hacktrimino);
        var result = doGoDown(hacktrimino);
        add(hacktrimino);
        return result;
    }

    @Override
    public void remove(Hacktrimino currentHacktrimino) {
        doRemove(currentHacktrimino);
    }

    private boolean canRotate(Hacktrimino hacktrimino) {
        var clone = hacktrimino.copy();
        clone.rotateRight();
        var points = clone.getPoints();
        for (Point point : points) {
            if (!grid.isEmptyCell(point.getY(), point.getX())) {
                return false;
            }
        }
        return true;
    }

    private void doGoLeft(Hacktrimino hacktrimino) {
        hacktrimino.goLeft();
        var points = hacktrimino.getPoints();
        for (Point point : points) {
            if (!grid.isEmptyCell(point.getY(), point.getX())) {
                hacktrimino.goRight();
                return;
            }
        }
    }

    private void doGoRight(Hacktrimino hacktrimino) {
        hacktrimino.goRight();
        var points = hacktrimino.getPoints();
        for (Point point : points) {
            if (!grid.isEmptyCell(point.getY(), point.getX())) {
                hacktrimino.goLeft();
                return;
            }
        }
    }

    private boolean doGoDown(Hacktrimino hacktrimino) {
        hacktrimino.goDown();
        var points = hacktrimino.getPoints();
        for (Point point : points) {
            if (!grid.isEmptyCell(point.getY(), point.getX())) {
                hacktrimino.goUp();
                return false;
            }
        }
        return true;
    }

    private void removeOnly(Hacktrimino hacktrimino) {
        if (canRemove(hacktrimino)) {
            doRemove(hacktrimino);
        }
    }

    private void refreshLinesClearedCounter(int linesCleared) {
        if (linesCleared > 0) {
            linesClearedEvent.setLinesCleared(linesCleared);
            publisherService.publishEvent(linesClearedEvent);
        }
    }

    private boolean isLineCleared(int y) {
        boolean isLineMade = true;
        for (int x = 0; x < propertiesService.gridMaxColumns() && isLineMade; x++) {
            isLineMade = !grid.isEmptyCell(y, x);
        }
        if (isLineMade) {
            log.info("Line made at y={}", y);
        }
        return isLineMade;
    }

    private void moveLinesToLine(int line) {
        for (int y = line; y > 0; y--) {
            for (int x = 0; x < propertiesService.gridMaxColumns(); x++) {
                if (grid.areInBounds(y - 1, x)) {
                    grid.setCell(y, x, grid.getCell(y - 1, x));
                }
            }
        }
        IntStream.range(0, propertiesService.gridMaxColumns())
                .filter(x -> grid.areInBounds(0, x))
                .forEach(x -> grid.setEmptyCell(0, x));
    }

    private boolean canRemove(Hacktrimino hacktrimino) {
        var points = hacktrimino.getPoints();
        for (Point point : points) {
            if (!grid.isEmptyCell(point.getX(), point.getY(), hacktrimino.getComposingCell())) {
                return false;
            }
        }
        return true;
    }

    private void doRemove(Hacktrimino currentHacktrimino) {
        currentHacktrimino
                .getPoints()
                .forEach(point -> grid.setEmptyCell(point.getY(), point.getX()));
        refreshGrid();
    }
}
