package fr.dmil.hacktris.domain.grids.main.services;

import fr.dmil.hacktris.domain.hacktriminos.models.Hacktrimino;

public interface HacktrisMainGridService {
    int clearLinesMade();

    void reset();

    void rotate(Hacktrimino hacktrimino);

    void goLeft(Hacktrimino hacktrimino);

    void goRight(Hacktrimino hacktrimino);

    boolean goDown(Hacktrimino hacktrimino);

    void remove(Hacktrimino hacktriminoToHold);
}
