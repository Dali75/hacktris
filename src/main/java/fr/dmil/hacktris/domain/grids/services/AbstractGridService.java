package fr.dmil.hacktris.domain.grids.services;

import fr.dmil.hacktris.domain.events.services.PublisherService;
import fr.dmil.hacktris.domain.grids.events.HacktrisGridChangedEvent;
import fr.dmil.hacktris.domain.grids.models.HacktriminoCell;
import fr.dmil.hacktris.domain.grids.models.HacktrisGrid;
import fr.dmil.hacktris.domain.hacktriminos.models.Hacktrimino;

public abstract class AbstractGridService {
    protected final HacktrisGrid grid;
    protected final PublisherService publisherService;
    private final HacktrisGridChangedEvent hacktrisGridChangedEvent;
    private final HacktriminoCell cell;

    protected AbstractGridService(
            int maxLines,
            int maxColumns,
            PublisherService publisherService,
            HacktrisGridChangedEvent hacktrisGridChangedEvent,
            HacktriminoCell cell) {
        this.hacktrisGridChangedEvent = hacktrisGridChangedEvent;
        this.grid = new HacktrisGrid(maxLines, maxColumns);
        this.publisherService = publisherService;
        this.cell = cell;
        initGrid();
    }

    protected void doReset() {
        initGrid();
        refreshGrid();
    }

    protected void initGrid() {
        for (int y = 0; y < grid.getMaxLines(); y++) {
            for (int x = 0; x < grid.getMaxColumns(); x++) {
                grid.setCell(y, x, cell);
            }
        }
    }

    protected void add(Hacktrimino hacktrimino) {
        hacktrimino
                .getPoints()
                .forEach(
                        point -> {
                            int y = point.getY();
                            int x = point.getX();
                            grid.setCell(y, x, hacktrimino.getComposingCell());
                        });
        refreshGrid();
    }

    protected void refreshGrid() {
        hacktrisGridChangedEvent.setGrid(grid);
        publisherService.publishEvent(hacktrisGridChangedEvent);
    }

    public void reset() {
        doReset();
    }
}
