package fr.dmil.hacktris.domain.grids.models;

import java.util.UUID;

public class HacktriminoICell extends HacktriminoCell {

    public HacktriminoICell(UUID id) {
        super(id, "I");
    }
}
