package fr.dmil.hacktris.domain.grids.main.events;

public interface LinesClearedEvent {
    void setLinesCleared(int linesCleared);
}
