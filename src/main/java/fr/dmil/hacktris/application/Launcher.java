package fr.dmil.hacktris.application;

import fr.dmil.hacktris.configuration.HacktrisConfiguration;
import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import(HacktrisConfiguration.class)
@SpringBootApplication(
        scanBasePackages = {
            "fr.dmil.hacktris.application",
            "fr.dmil.hacktris.configuration",
            "fr.dmil.hacktris.infrastructure"
        })
public class Launcher {
    public static void main(String[] args) {
        Application.launch(HacktrisApplication.class);
    }
}
