package fr.dmil.hacktris.application.controllers.services;

import javafx.scene.input.KeyEvent;

public interface HacktrisControllerService {
    void start();

    void stop();

    void handle(KeyEvent keyEvent);
}
