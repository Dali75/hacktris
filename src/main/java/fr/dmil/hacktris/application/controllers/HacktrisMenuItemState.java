package fr.dmil.hacktris.application.controllers;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@Component
public class HacktrisMenuItemState {
    private final BooleanProperty stop;
    private final BooleanProperty start;

    public HacktrisMenuItemState() {
        this.start = new SimpleBooleanProperty(false);
        this.stop = new SimpleBooleanProperty(true);
    }

    public void setStart(boolean start) {
        this.start.set(start);
    }

    public void setStop(boolean stop) {
        this.stop.set(stop);
    }
}
