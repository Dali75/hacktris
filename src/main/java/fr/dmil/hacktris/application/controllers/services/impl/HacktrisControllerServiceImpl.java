package fr.dmil.hacktris.application.controllers.services.impl;

import static fr.dmil.hacktris.domain.hacktris.enums.HacktrisStatus.*;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import fr.dmil.hacktris.application.controllers.HacktrisMenuItemState;
import fr.dmil.hacktris.application.controllers.services.HacktrisControllerService;
import fr.dmil.hacktris.application.multimedia.graphics.linescleared.services.LinesClearedService;
import fr.dmil.hacktris.application.multimedia.graphics.stopwatch.services.StopWatchService;
import fr.dmil.hacktris.application.multimedia.sounds.services.SoundService;
import fr.dmil.hacktris.domain.hacktris.services.HacktrisService;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class HacktrisControllerServiceImpl
        implements HacktrisControllerService, ApplicationContextAware {
    private ApplicationContext applicationContext;
    private final SoundService soundService;
    private final HacktrisService hacktrisService;
    private final StopWatchService stopWatchService;
    private final LinesClearedService linesClearedService;
    private final HacktrisMenuItemState hacktrisMenuItemState;
    private ScheduledExecutorService scheduledExecutorService;

    @Override
    public void start() {
        log.info("Starting the game !!!");
        stopWatchService.start();
        linesClearedService.reset();
        hacktrisMenuItemState.setStart(true);
        hacktrisMenuItemState.setStop(false);
        hacktrisService.start();
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        Platform.runLater(
                () ->
                        scheduledExecutorService.scheduleAtFixedRate(
                                () -> {
                                    if (!hacktrisService.isGamePaused()) {
                                        var status = hacktrisService.down();
                                        if (status == GAME_OVER) {
                                            doStop();
                                        }
                                        if (hacktrisService.shouldPlayFallSound(status)) {
                                            soundService.play("fx_fall.mp3");
                                        }
                                    }
                                },
                                0,
                                1000,
                                MILLISECONDS));
    }

    @Override
    public void stop() {
        doStop();
    }

    @Override
    public void handle(KeyEvent keyEvent) {
        Platform.runLater(
                () -> {
                    var code = keyEvent.getCode();
                    var isGameRunning = hacktrisService.isGameRunning();
                    var isGamePaused = hacktrisService.isGamePaused();
                    if (!isGameRunning || isGamePaused && code != KeyCode.P) {
                        return;
                    }

                    if (!isGamePaused && code == KeyCode.P) {
                        stopWatchService.pause();
                    } else if (isGamePaused) {
                        stopWatchService.restart();
                    }

                    Arrays.stream(KeyPressed.values())
                            .filter(keyPressed -> keyPressed.getKeyCode() == code)
                            .findFirst()
                            .ifPresent(keyPressed -> keyPressed.process(applicationContext));
                });
    }

    private void doStop() {
        log.info("Game over, stopping the game");
        stopWatchService.stop();
        hacktrisMenuItemState.setStart(false);
        hacktrisMenuItemState.setStop(true);
        hacktrisService.stop();
        if (scheduledExecutorService != null) {
            scheduledExecutorService.shutdown();
        }
    }

    @Override
    public void setApplicationContext(@NonNull ApplicationContext applicationContext)
            throws BeansException {
        this.applicationContext = applicationContext;
    }
}
