package fr.dmil.hacktris.application.controllers.services.impl;

import fr.dmil.hacktris.application.multimedia.sounds.services.SoundService;
import fr.dmil.hacktris.domain.hacktris.services.HacktrisService;
import javafx.scene.input.KeyCode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

@Slf4j
@Getter
enum KeyPressed {
    UP(KeyCode.UP) {
        @Override
        public void processAction() {
            hacktrisService.rotate();
        }
    },
    LEFT(KeyCode.LEFT) {
        @Override
        public void processAction() {
            hacktrisService.left();
        }
    },
    RIGHT(KeyCode.RIGHT) {
        @Override
        public void processAction() {
            hacktrisService.right();
        }
    },
    DOWN(KeyCode.DOWN) {
        @Override
        public void processAction() {
            var status = hacktrisService.down();
            if (hacktrisService.shouldPlayFallSound(status)) {
                soundService.play("fx_fall.mp3");
            }
        }
    },
    SPACE(KeyCode.SPACE) {
        @Override
        public void processAction() {
            var status = hacktrisService.hardDrop();
            if (hacktrisService.shouldPlayFallSound(status)) {
                soundService.play("fx_fall.mp3");
            }
        }
    },
    H(KeyCode.H) {
        @Override
        public void processAction() {
            hacktrisService.hold();
        }
    },
    P(KeyCode.P) {
        @Override
        public void processAction() {
            hacktrisService.pauseOrResume();
        }
    };

    private final KeyCode keyCode;
    protected SoundService soundService;
    protected HacktrisService hacktrisService;

    KeyPressed(KeyCode keyCode) {
        this.keyCode = keyCode;
    }

    public abstract void processAction();

    public void process(ApplicationContext applicationContext) {
        this.hacktrisService = applicationContext.getBean(HacktrisService.class);
        this.soundService = applicationContext.getBean(SoundService.class);

        log.info("{} key pressed !!!", getKeyCode().getName());
        processAction();
    }
}
