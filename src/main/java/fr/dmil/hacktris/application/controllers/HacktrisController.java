package fr.dmil.hacktris.application.controllers;

import fr.dmil.hacktris.application.controllers.services.HacktrisControllerService;
import fr.dmil.hacktris.application.events.RefreshComponentEvent;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.impl.HacktrisHoldGridVBox;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.impl.HacktrisNextGridVBox;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.main.impl.HacktrisMainGridVBox;
import fr.dmil.hacktris.application.multimedia.graphics.linescleared.vbox.LinesClearedVBox;
import fr.dmil.hacktris.application.multimedia.graphics.stopwatch.vbox.StopWatchVBox;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Slf4j
@FxmlView
@Component
@RequiredArgsConstructor
public class HacktrisController implements ApplicationListener<RefreshComponentEvent> {

    @FXML private AnchorPane anchorPane;
    @FXML private MenuItem menuItemStart;
    @FXML private MenuItem menuItemStop;
    private final StopWatchVBox stopWatchVBox;
    private final LinesClearedVBox linesClearedVBox;
    private final HacktrisMainGridVBox hacktrisMainGridVBox;
    private final HacktrisNextGridVBox hacktrisNextGridVBox;
    private final HacktrisHoldGridVBox hacktrisHoldGridVBox;
    private final HacktrisMenuItemState hacktrisMenuItemState;
    private final HacktrisControllerService hacktrisControllerService;

    @FXML
    private void initialize() {
        log.info("Initializing the main controller !!");
        anchorPane.getChildren().add(hacktrisMainGridVBox);
        anchorPane.getChildren().add(hacktrisHoldGridVBox);
        anchorPane.getChildren().add(hacktrisNextGridVBox);
        anchorPane.getChildren().add(linesClearedVBox);
        anchorPane.getChildren().add(stopWatchVBox);
        menuItemStart.disableProperty().bindBidirectional(hacktrisMenuItemState.getStart());
        menuItemStop.disableProperty().bindBidirectional(hacktrisMenuItemState.getStop());
    }

    @FXML
    public void start() {
        log.info("Click on start menu item !!");
        hacktrisControllerService.start();
    }

    @FXML
    public void stop() {
        log.info("Click on stop menu item !!");
        hacktrisControllerService.stop();
    }

    public void handle(KeyEvent keyEvent) {
        hacktrisControllerService.handle(keyEvent);
    }

    @Override
    public void onApplicationEvent(@NonNull RefreshComponentEvent refreshComponentEvent) {
        refreshComponentEvent.process();
    }
}
