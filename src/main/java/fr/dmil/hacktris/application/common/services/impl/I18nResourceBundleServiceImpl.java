package fr.dmil.hacktris.application.common.services.impl;

import static java.util.ResourceBundle.getBundle;

import fr.dmil.hacktris.application.common.services.I18nResourceBundleService;
import java.util.Locale;
import java.util.ResourceBundle;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Getter
@Service
@RequiredArgsConstructor
public class I18nResourceBundleServiceImpl implements I18nResourceBundleService {
    public static final String MESSAGES = "messages";
    private final ResourceBundle resourceBundle = getBundle(MESSAGES, Locale.getDefault());

    @Override
    public String getMsg(String key) {
        return resourceBundle.getString(key);
    }
}
