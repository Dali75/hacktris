package fr.dmil.hacktris.application.common.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ComponentPosition {
    public static final double HACKTRIS_GRID_X = 122;
    public static final double HACKTRIS_HOLDGRID_X = 20;
    public static final double HACKTRIS_NEXTGRID_X = 344;
    public static final double GAME_ROOT_Y = 44;
    public static final double TIME_BOX_X = 344;
    public static final double TIME_BOX_Y = GAME_ROOT_Y + 106;

    public static final double LINES_CLEARED_X = HACKTRIS_HOLDGRID_X;
    public static final double LINES_CLEARED_Y = GAME_ROOT_Y + 106;
}
