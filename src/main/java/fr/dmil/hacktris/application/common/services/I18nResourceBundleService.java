package fr.dmil.hacktris.application.common.services;

import java.util.ResourceBundle;

public interface I18nResourceBundleService {
    String getMsg(String key);

    ResourceBundle getResourceBundle();
}
