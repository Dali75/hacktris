package fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.main.impl;

import fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.AbstractHacktrisGridCanvas;
import fr.dmil.hacktris.infrastructure.properties.HacktrisGridProperties;
import javafx.application.Platform;
import javafx.scene.image.Image;
import org.springframework.stereotype.Component;

@Component
public class HacktrisMainGridCanvas extends AbstractHacktrisGridCanvas {

    public HacktrisMainGridCanvas(HacktrisGridProperties hacktrisGridProperties) {
        super(
                TWENTY * hacktrisGridProperties.getMaxColumns(),
                TWENTY * hacktrisGridProperties.getMaxLines(),
                hacktrisGridProperties.getMaxLines(),
                hacktrisGridProperties.getMaxColumns());
    }

    @Override
    public void draw(Image[][] images) {
        Platform.runLater(
                () -> {
                    for (int y = 0; y < maxLines; y++) {
                        for (int x = 0; x < maxColumns; x++) {
                            graphicsContext.drawImage(images[y][x], x * TWENTY, y * TWENTY);
                        }
                    }
                });
    }
}
