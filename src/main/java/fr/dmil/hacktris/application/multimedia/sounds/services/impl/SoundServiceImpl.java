package fr.dmil.hacktris.application.multimedia.sounds.services.impl;

import fr.dmil.hacktris.application.multimedia.sounds.services.SoundService;
import javafx.scene.media.AudioClip;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SoundServiceImpl implements SoundService {
    private static final String SOUNDS_BASE_DIR = "sounds/%s";
    private final ResourceLoader resourceLoader;

    @Override
    @SneakyThrows
    public void play(String fileName) {
        var location = SOUNDS_BASE_DIR.formatted(fileName);
        var resource = resourceLoader.getResource(location);
        var audioClip = new AudioClip(resource.getURI().toString());
        audioClip.play();
    }
}
