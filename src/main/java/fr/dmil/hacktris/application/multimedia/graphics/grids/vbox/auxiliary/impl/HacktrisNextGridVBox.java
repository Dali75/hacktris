package fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.impl;

import static fr.dmil.hacktris.application.common.constants.ComponentPosition.*;

import fr.dmil.hacktris.application.common.services.I18nResourceBundleService;
import fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.auxiliary.impl.HacktrisNextGridCanvas;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.AbstractHacktrisAuxiliaryGridVBox;
import org.springframework.stereotype.Component;

@Component
public class HacktrisNextGridVBox extends AbstractHacktrisAuxiliaryGridVBox {
    private static final String NEXT_GRID_TITLE = "next.grid.title";

    public HacktrisNextGridVBox(
            HacktrisNextGridCanvas hacktrisNextGridCanvas,
            I18nResourceBundleService i18nResourceBundleService) {
        super(
                HACKTRIS_NEXTGRID_X,
                GAME_ROOT_Y,
                hacktrisNextGridCanvas,
                i18nResourceBundleService.getMsg(NEXT_GRID_TITLE));
    }
}
