package fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.auxiliary.impl;

import fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.auxiliary.AbstractHacktrisAuxiliaryGridCanvas;
import fr.dmil.hacktris.infrastructure.properties.HoldGridProperties;
import org.springframework.stereotype.Component;

@Component
public class HacktrisHoldGridCanvas extends AbstractHacktrisAuxiliaryGridCanvas {

    public HacktrisHoldGridCanvas(HoldGridProperties holdGridProperties) {
        super(
                TWENTY * holdGridProperties.getMaxColumns(),
                TWENTY * holdGridProperties.getMaxLines(),
                holdGridProperties.getMaxLines(),
                holdGridProperties.getMaxColumns());
    }
}
