package fr.dmil.hacktris.application.multimedia.graphics.services;

import fr.dmil.hacktris.domain.grids.models.HacktrisGrid;
import javafx.scene.image.Image;

public interface ImageService {
    Image[][] getImagesGrid(HacktrisGrid grid);

    Image getEmptyCell();
}
