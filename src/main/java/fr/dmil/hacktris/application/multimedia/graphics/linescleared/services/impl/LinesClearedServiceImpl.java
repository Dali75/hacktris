package fr.dmil.hacktris.application.multimedia.graphics.linescleared.services.impl;

import fr.dmil.hacktris.application.multimedia.graphics.linescleared.services.LinesClearedService;
import fr.dmil.hacktris.application.multimedia.graphics.linescleared.vbox.LinesClearedVBox;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LinesClearedServiceImpl implements LinesClearedService {
    private final LinesClearedVBox linesClearedVBox;

    @Override
    public void reset() {
        linesClearedVBox.reset();
    }
}
