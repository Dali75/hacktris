package fr.dmil.hacktris.application.multimedia.graphics.stopwatch.services.impl;

import fr.dmil.hacktris.application.multimedia.graphics.stopwatch.services.StopWatchService;
import fr.dmil.hacktris.application.multimedia.graphics.stopwatch.vbox.StopWatchVBox;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class StopWatchServiceImpl implements StopWatchService {
    private final StopWatchVBox stopWatchVBox;

    @Override
    public void start() {
        stopWatchVBox.start();
    }

    @Override
    public void stop() {
        stopWatchVBox.stop();
    }

    @Override
    public void pause() {
        stopWatchVBox.pause();
    }

    @Override
    public void restart() {
        stopWatchVBox.restart();
    }
}
