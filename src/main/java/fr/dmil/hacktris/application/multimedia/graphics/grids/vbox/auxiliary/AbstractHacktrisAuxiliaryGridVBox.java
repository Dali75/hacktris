package fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary;

import fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.GridCanvas;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.AbstractGridVBox;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public abstract class AbstractHacktrisAuxiliaryGridVBox extends AbstractGridVBox {

    private final GridCanvas gridCanvas;

    protected AbstractHacktrisAuxiliaryGridVBox(
            double x, double y, GridCanvas gridCanvas, String title) {
        super(x, y);
        this.gridCanvas = gridCanvas;
        getChildren().add(vBox(title));

        var maxLines = gridCanvas.getMaxLines();
        var maxColumns = gridCanvas.getMaxColumns();
        gridCanvas.draw(nullCells(maxLines, maxColumns));
    }

    @Override
    public void draw(Image[][] images) {
        gridCanvas.draw(images);
    }

    private VBox vBox(String title) {
        var vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10.0);
        vBox.getChildren().addAll(text(title), (Canvas) gridCanvas);
        return vBox;
    }

    private static Text text(String title) {
        var label = new Text(title);
        label.setStyle("-fx-font-weight: bold;");
        return label;
    }

    private Image[][] nullCells(int maxLines, int maxColumns) {
        Image[][] images = new Image[maxLines][maxColumns];
        for (int y = 0; y < maxLines; y++) {
            for (int x = 0; x < maxColumns; x++) {
                images[y][x] = null;
            }
        }
        return images;
    }
}
