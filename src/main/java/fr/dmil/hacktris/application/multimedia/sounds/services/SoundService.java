package fr.dmil.hacktris.application.multimedia.sounds.services;

public interface SoundService {
    void play(String fileName);
}
