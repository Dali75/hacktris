package fr.dmil.hacktris.application.multimedia.graphics.grids.canvas;

import javafx.scene.image.Image;

public interface GridCanvas {
    void draw(Image[][] images);

    int getMaxLines();

    int getMaxColumns();
}
