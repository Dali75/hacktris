package fr.dmil.hacktris.application.multimedia.graphics.stage;

import fr.dmil.hacktris.application.common.services.I18nResourceBundleService;
import fr.dmil.hacktris.application.controllers.HacktrisController;
import fr.dmil.hacktris.application.multimedia.graphics.stage.events.StageReadyEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import lombok.RequiredArgsConstructor;
import net.rgielen.fxweaver.core.FxWeaver;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class PrimaryStageInitializer implements ApplicationListener<StageReadyEvent> {
    private static final String TITLE = "title";
    private static final String SKULL_ICON = "/images/skull.gif";
    private static final int WIDTH = 445;
    private static final int LENGTH = 465;
    private final FxWeaver fxWeaver;
    private final HacktrisController hacktrisController;
    private final I18nResourceBundleService i18NResourceBundleService;

    @Override
    public void onApplicationEvent(StageReadyEvent event) {
        var stage = event.stage;
        stage.setScene(scene());
        stage.setResizable(false);
        stage.setTitle(i18NResourceBundleService.getMsg(TITLE));
        stage.getIcons().add(new Image(SKULL_ICON));
        stage.show();
    }

    private Scene scene() {
        var resourceBundle = i18NResourceBundleService.getResourceBundle();
        Parent parent = fxWeaver.loadView(HacktrisController.class, resourceBundle);
        var scene = new Scene(parent, WIDTH, LENGTH);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, hacktrisController::handle);
        return scene;
    }
}
