package fr.dmil.hacktris.application.multimedia.graphics.linescleared.vbox;

import static fr.dmil.hacktris.application.common.constants.ComponentPosition.*;

import fr.dmil.hacktris.application.common.services.I18nResourceBundleService;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Component;

@Component
public class LinesClearedVBox extends VBox {
    private static final String LINES_CLEARED_TITLE = "lines.cleared.title";
    private int counter;
    private Label linesCleared;

    public LinesClearedVBox(I18nResourceBundleService i18nResourceBundleService) {
        super();
        init(i18nResourceBundleService.getMsg(LINES_CLEARED_TITLE));
    }

    private void init(String title) {
        setLayoutX(LINES_CLEARED_X);
        setLayoutY(LINES_CLEARED_Y);
        this.setSpacing(10);
        this.setAlignment(Pos.CENTER);
        var label = new Label(title);
        label.setStyle("-fx-font-weight: bold;");
        this.linesCleared = new Label(String.valueOf(counter));
        this.getChildren().addAll(label, linesCleared);
    }

    public void update(int linesMade) {
        this.counter += linesMade;
        Platform.runLater(() -> linesCleared.setText(String.valueOf(this.counter)));
    }

    public void reset() {
        this.counter = 0;
        Platform.runLater(() -> linesCleared.setText(String.valueOf(this.counter)));
    }
}
