package fr.dmil.hacktris.application.multimedia.graphics.services.impl;

import fr.dmil.hacktris.application.multimedia.graphics.services.ImageService;
import fr.dmil.hacktris.domain.grids.models.*;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.image.Image;
import org.springframework.stereotype.Service;

@Service
public class ImageServiceImpl implements ImageService {
    private static final String CELL_IMAGE_PATTERN_URL = "/images/cells/%s.jpg";
    private final Map<Class<? extends HacktriminoCell>, Image> cellImages;

    public ImageServiceImpl() {
        cellImages = new HashMap<>();
        cellImages.put(HacktriminoICell.class, getImage(HacktriminoICell.class));
        cellImages.put(HacktriminoJCell.class, getImage(HacktriminoJCell.class));
        cellImages.put(HacktriminoLCell.class, getImage(HacktriminoLCell.class));
        cellImages.put(HacktriminoOCell.class, getImage(HacktriminoOCell.class));
        cellImages.put(HacktriminoSCell.class, getImage(HacktriminoSCell.class));
        cellImages.put(HacktriminoTCell.class, getImage(HacktriminoTCell.class));
        cellImages.put(HacktriminoZCell.class, getImage(HacktriminoZCell.class));
        cellImages.put(HacktriminoEmptyCell.class, getImage(HacktriminoEmptyCell.class));
    }

    @Override
    public Image[][] getImagesGrid(HacktrisGrid grid) {
        var images = new Image[grid.getMaxLines()][grid.getMaxColumns()];
        for (int y = 0; y < grid.getMaxLines(); y++) {
            for (int x = 0; x < grid.getMaxColumns(); x++) {
                var cell = grid.getCell(y, x);
                if (cell != null) {
                    images[y][x] = cellImages.get(cell.getClass());
                } else {
                    images[y][x] = null;
                }
            }
        }
        return images;
    }

    @Override
    public Image getEmptyCell() {
        return cellImages.get(HacktriminoEmptyCell.class);
    }

    private static Image getImage(Class<? extends HacktriminoCell> clazz) {
        return new Image(CELL_IMAGE_PATTERN_URL.formatted(clazz.getSimpleName()));
    }
}
