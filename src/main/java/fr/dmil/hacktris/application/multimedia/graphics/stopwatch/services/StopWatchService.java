package fr.dmil.hacktris.application.multimedia.graphics.stopwatch.services;

public interface StopWatchService {
    void start();

    void stop();

    void pause();

    void restart();
}
