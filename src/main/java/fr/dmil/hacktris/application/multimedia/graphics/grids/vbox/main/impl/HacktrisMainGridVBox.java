package fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.main.impl;

import static fr.dmil.hacktris.application.common.constants.ComponentPosition.GAME_ROOT_Y;
import static fr.dmil.hacktris.application.common.constants.ComponentPosition.HACKTRIS_GRID_X;

import fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.main.impl.HacktrisMainGridCanvas;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.AbstractGridVBox;
import fr.dmil.hacktris.application.multimedia.graphics.services.ImageService;
import javafx.scene.image.Image;
import org.springframework.stereotype.Component;

@Component
public class HacktrisMainGridVBox extends AbstractGridVBox {
    private final ImageService imageService;
    private final HacktrisMainGridCanvas hacktrisMainGridCanvas;

    public HacktrisMainGridVBox(
            ImageService imageService, HacktrisMainGridCanvas hacktrisMainGridCanvas) {
        super(HACKTRIS_GRID_X, GAME_ROOT_Y);
        this.imageService = imageService;
        this.hacktrisMainGridCanvas = hacktrisMainGridCanvas;
        init();
    }

    private void init() {
        setStyle(
                """
                -fx-background-color: black;
                -fx-border-color: black;
                -fx-border-radius: 5px;
                """);
        getChildren().add(this.hacktrisMainGridCanvas);

        hacktrisMainGridCanvas.draw(emptyCells());
    }

    @Override
    public void draw(Image[][] grid) {
        hacktrisMainGridCanvas.draw(grid);
    }

    private Image[][] emptyCells() {
        var maxLines = hacktrisMainGridCanvas.getMaxLines();
        var maxColumns = hacktrisMainGridCanvas.getMaxColumns();
        Image[][] images = new Image[maxLines][maxColumns];
        for (int i = 0; i < maxLines; i++) {
            for (int j = 0; j < maxColumns; j++) {
                images[i][j] = imageService.getEmptyCell();
            }
        }
        return images;
    }
}
