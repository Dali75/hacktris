package fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.impl;

import static fr.dmil.hacktris.application.common.constants.ComponentPosition.*;

import fr.dmil.hacktris.application.common.services.I18nResourceBundleService;
import fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.auxiliary.impl.HacktrisHoldGridCanvas;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.AbstractHacktrisAuxiliaryGridVBox;
import org.springframework.stereotype.Component;

@Component
public class HacktrisHoldGridVBox extends AbstractHacktrisAuxiliaryGridVBox {
    private static final String HOLD_GRID_TITLE = "hold.grid.title";

    public HacktrisHoldGridVBox(
            HacktrisHoldGridCanvas hacktrisHoldGridCanvas,
            I18nResourceBundleService i18nResourceBundleService) {
        super(
                HACKTRIS_HOLDGRID_X,
                GAME_ROOT_Y,
                hacktrisHoldGridCanvas,
                i18nResourceBundleService.getMsg(HOLD_GRID_TITLE));
    }
}
