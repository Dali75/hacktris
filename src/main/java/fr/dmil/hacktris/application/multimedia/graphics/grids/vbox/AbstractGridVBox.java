package fr.dmil.hacktris.application.multimedia.graphics.grids.vbox;

import javafx.scene.layout.VBox;

public abstract class AbstractGridVBox extends VBox implements GridVBox {
    protected AbstractGridVBox(double x, double y) {
        setLayoutX(x);
        setLayoutY(y);
    }
}
