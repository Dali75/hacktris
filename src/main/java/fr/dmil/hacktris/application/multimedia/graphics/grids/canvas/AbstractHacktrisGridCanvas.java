package fr.dmil.hacktris.application.multimedia.graphics.grids.canvas;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractHacktrisGridCanvas extends Canvas implements GridCanvas {
    protected static final double TWENTY = 20.0;
    @Getter protected final int maxLines;
    @Getter protected final int maxColumns;
    protected final GraphicsContext graphicsContext;

    protected AbstractHacktrisGridCanvas(
            double width, double height, int maxLines, int maxColumns) {
        super(width, height);
        this.maxLines = maxLines;
        this.maxColumns = maxColumns;
        this.graphicsContext = getGraphicsContext2D();
    }
}
