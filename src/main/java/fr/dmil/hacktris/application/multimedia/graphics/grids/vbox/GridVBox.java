package fr.dmil.hacktris.application.multimedia.graphics.grids.vbox;

import javafx.scene.image.Image;

public interface GridVBox {
    void draw(Image[][] images);
}
