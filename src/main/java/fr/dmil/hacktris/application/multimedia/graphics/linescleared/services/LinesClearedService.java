package fr.dmil.hacktris.application.multimedia.graphics.linescleared.services;

public interface LinesClearedService {
    void reset();
}
