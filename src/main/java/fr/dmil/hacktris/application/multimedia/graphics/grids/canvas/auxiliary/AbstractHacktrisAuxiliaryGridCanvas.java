package fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.auxiliary;

import fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.AbstractHacktrisGridCanvas;
import javafx.application.Platform;
import javafx.scene.image.Image;

public abstract class AbstractHacktrisAuxiliaryGridCanvas extends AbstractHacktrisGridCanvas {

    protected AbstractHacktrisAuxiliaryGridCanvas(
            double width, double height, int maxLines, int maxColumns) {
        super(width, height, maxLines, maxColumns);
    }

    @Override
    public void draw(Image[][] images) {
        Platform.runLater(
                () -> {
                    for (int y = 0; y < maxLines; y++) {
                        for (int x = 0; x < maxColumns; x++) {
                            var img = images[y][x];
                            var width = x * TWENTY;
                            var height = y * TWENTY;
                            if (img != null) {
                                graphicsContext.drawImage(img, width, height);
                            } else {
                                graphicsContext.clearRect(width, height, TWENTY, TWENTY);
                            }
                        }
                    }
                });
    }
}
