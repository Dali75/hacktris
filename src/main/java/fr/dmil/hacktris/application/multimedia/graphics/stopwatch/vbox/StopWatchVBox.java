package fr.dmil.hacktris.application.multimedia.graphics.stopwatch.vbox;

import static fr.dmil.hacktris.application.common.constants.ComponentPosition.TIME_BOX_X;
import static fr.dmil.hacktris.application.common.constants.ComponentPosition.TIME_BOX_Y;

import fr.dmil.hacktris.application.common.services.I18nResourceBundleService;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import org.springframework.stereotype.Component;

@Component
public class StopWatchVBox extends VBox {
    private static final String START_TIME = "00:00:00:000";
    private static final String HHMMSSMMM = "%02d:%02d:%02d:%03d";
    private static final String STOPWATCH_TITLE = "stop.watch.title";

    private long startTime;
    private long pauseTime;
    private Timeline timeline;
    private Label chronometer;

    public StopWatchVBox(I18nResourceBundleService i18nResourceBundleService) {
        super();
        init(i18nResourceBundleService.getMsg(STOPWATCH_TITLE));
    }

    private void init(String title) {
        setLayoutX(TIME_BOX_X);
        setLayoutY(TIME_BOX_Y);
        this.setSpacing(10);
        this.setAlignment(Pos.CENTER);
        var label = new Label(title);
        label.setStyle("-fx-font-weight: bold;");
        this.chronometer = new Label(START_TIME);
        this.getChildren().addAll(label, chronometer);
    }

    public void start() {
        Platform.runLater(() -> chronometer.setText(START_TIME));
        startTime = System.currentTimeMillis();
        timeline = new Timeline(new KeyFrame(Duration.millis(100), event -> updateTime()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    public void pause() {
        timeline.pause();
        pauseTime = System.currentTimeMillis();
    }

    public void stop() {
        if (timeline != null) {
            timeline.stop();
        }
    }

    public void restart() {
        timeline.stop();
        startTime += System.currentTimeMillis() - pauseTime;
        timeline.play();
    }

    private void updateTime() {
        long currentTime;
        if (timeline.getStatus() == Animation.Status.PAUSED) {
            currentTime = pauseTime;
        } else {
            currentTime = System.currentTimeMillis();
        }

        long duration = currentTime - startTime;
        int hours = (int) (duration / 3600000);
        int minutes = (int) ((duration % 3600000) / 60000);
        int seconds = (int) ((duration % 60000) / 1000);
        int milliseconds = (int) (duration % 1000);

        var formattedTime = String.format(HHMMSSMMM, hours, minutes, seconds, milliseconds);
        Platform.runLater(() -> chronometer.setText(formattedTime));
    }
}
