package fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.auxiliary.impl;

import fr.dmil.hacktris.application.multimedia.graphics.grids.canvas.auxiliary.AbstractHacktrisAuxiliaryGridCanvas;
import fr.dmil.hacktris.infrastructure.properties.NextGridProperties;
import org.springframework.stereotype.Component;

@Component
public class HacktrisNextGridCanvas extends AbstractHacktrisAuxiliaryGridCanvas {

    public HacktrisNextGridCanvas(NextGridProperties nextGridProperties) {
        super(
                TWENTY * nextGridProperties.getMaxColumns(),
                TWENTY * nextGridProperties.getMaxLines(),
                nextGridProperties.getMaxLines(),
                nextGridProperties.getMaxColumns());
    }
}
