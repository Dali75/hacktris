package fr.dmil.hacktris.application;

import fr.dmil.hacktris.application.controllers.HacktrisController;
import fr.dmil.hacktris.application.multimedia.graphics.stage.events.StageReadyEvent;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

public class HacktrisApplication extends Application {
    private ConfigurableApplicationContext applicationContext;

    @Override
    public void init() {
        applicationContext = new SpringApplicationBuilder().sources(Launcher.class).run();
    }

    @Override
    public void start(Stage primaryStage) {
        applicationContext.publishEvent(new StageReadyEvent(primaryStage));
    }

    @Override
    public void stop() {
        applicationContext.getBean(HacktrisController.class).stop();
        applicationContext.close();
        Platform.exit();
    }
}
