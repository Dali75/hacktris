package fr.dmil.hacktris.application.events.grids;

import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.main.impl.HacktrisMainGridVBox;
import fr.dmil.hacktris.application.multimedia.graphics.services.ImageService;
import fr.dmil.hacktris.domain.grids.main.events.HacktrisMainGridChangedEvent;
import org.springframework.stereotype.Component;

@Component
public class HacktrisMainGridCanvasRefresh extends HacktrisGridCanvasRefresh
        implements HacktrisMainGridChangedEvent {

    public HacktrisMainGridCanvasRefresh(HacktrisMainGridVBox gridVBox, ImageService imageService) {
        super(gridVBox, imageService);
    }
}
