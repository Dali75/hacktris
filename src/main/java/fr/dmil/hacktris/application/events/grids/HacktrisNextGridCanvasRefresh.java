package fr.dmil.hacktris.application.events.grids;

import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.impl.HacktrisNextGridVBox;
import fr.dmil.hacktris.application.multimedia.graphics.services.ImageService;
import fr.dmil.hacktris.domain.grids.auxiliary.events.HacktrisNextGridChangedEvent;
import org.springframework.stereotype.Component;

@Component
public class HacktrisNextGridCanvasRefresh extends HacktrisGridCanvasRefresh
        implements HacktrisNextGridChangedEvent {

    public HacktrisNextGridCanvasRefresh(HacktrisNextGridVBox gridVBox, ImageService imageService) {
        super(gridVBox, imageService);
    }
}
