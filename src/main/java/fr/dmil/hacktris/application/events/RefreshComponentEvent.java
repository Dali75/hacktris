package fr.dmil.hacktris.application.events;

import org.springframework.context.ApplicationEvent;

public abstract class RefreshComponentEvent extends ApplicationEvent {
    protected RefreshComponentEvent() {
        super("");
    }

    public abstract void process();
}
