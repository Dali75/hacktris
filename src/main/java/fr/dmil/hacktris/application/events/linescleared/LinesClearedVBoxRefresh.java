package fr.dmil.hacktris.application.events.linescleared;

import fr.dmil.hacktris.application.events.RefreshComponentEvent;
import fr.dmil.hacktris.application.multimedia.graphics.linescleared.vbox.LinesClearedVBox;
import fr.dmil.hacktris.domain.grids.main.events.LinesClearedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LinesClearedVBoxRefresh extends RefreshComponentEvent implements LinesClearedEvent {
    private int linesCleared;
    private final transient LinesClearedVBox linesClearedVBox;

    @Override
    public void process() {
        linesClearedVBox.update(linesCleared);
    }

    @Override
    public void setLinesCleared(int linesCleared) {
        this.linesCleared = linesCleared;
    }
}
