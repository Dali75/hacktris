package fr.dmil.hacktris.application.events.grids;

import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.auxiliary.impl.HacktrisHoldGridVBox;
import fr.dmil.hacktris.application.multimedia.graphics.services.ImageService;
import fr.dmil.hacktris.domain.grids.auxiliary.events.HacktrisHoldGridChangedEvent;
import org.springframework.stereotype.Component;

@Component
public class HacktrisHoldGridCanvasRefresh extends HacktrisGridCanvasRefresh
        implements HacktrisHoldGridChangedEvent {

    public HacktrisHoldGridCanvasRefresh(HacktrisHoldGridVBox gridVBox, ImageService imageService) {
        super(gridVBox, imageService);
    }
}
