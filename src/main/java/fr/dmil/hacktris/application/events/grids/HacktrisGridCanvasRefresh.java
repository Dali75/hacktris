package fr.dmil.hacktris.application.events.grids;

import fr.dmil.hacktris.application.events.RefreshComponentEvent;
import fr.dmil.hacktris.application.multimedia.graphics.grids.vbox.GridVBox;
import fr.dmil.hacktris.application.multimedia.graphics.services.ImageService;
import fr.dmil.hacktris.domain.grids.models.HacktrisGrid;
import lombok.Setter;

public class HacktrisGridCanvasRefresh extends RefreshComponentEvent {
    @Setter private transient HacktrisGrid grid;
    private final transient GridVBox gridVBox;
    private final transient ImageService imageService;

    public HacktrisGridCanvasRefresh(GridVBox gridVBox, ImageService imageService) {
        this.gridVBox = gridVBox;
        this.imageService = imageService;
    }

    @Override
    public void process() {
        gridVBox.draw(imageService.getImagesGrid(grid));
    }
}
