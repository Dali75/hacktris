package fr.dmil.hacktris.infrastructure.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "next-grid")
public class NextGridProperties {
    private int maxLines;
    private int maxColumns;
}
