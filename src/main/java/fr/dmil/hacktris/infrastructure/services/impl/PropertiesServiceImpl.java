package fr.dmil.hacktris.infrastructure.services.impl;

import fr.dmil.hacktris.domain.properties.services.PropertiesService;
import fr.dmil.hacktris.infrastructure.properties.HacktriminoProperties;
import fr.dmil.hacktris.infrastructure.properties.HacktrisGridProperties;
import fr.dmil.hacktris.infrastructure.properties.HoldGridProperties;
import fr.dmil.hacktris.infrastructure.properties.NextGridProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PropertiesServiceImpl implements PropertiesService {
    private final NextGridProperties nextGridProperties;
    private final HoldGridProperties holdGridProperties;
    private final HacktriminoProperties hacktriminoProperties;
    private final HacktrisGridProperties hacktrisGridProperties;

    @Override
    public int startingLine() {
        return hacktriminoProperties.getStartingLine();
    }

    @Override
    public int gridMaxLines() {
        return hacktrisGridProperties.getMaxLines();
    }

    @Override
    public int gridMaxColumns() {
        return hacktrisGridProperties.getMaxColumns();
    }

    @Override
    public int holdGridMaxLines() {
        return holdGridProperties.getMaxLines();
    }

    @Override
    public int holdGridMaxColumns() {
        return holdGridProperties.getMaxColumns();
    }

    @Override
    public int nextGridMaxLines() {
        return nextGridProperties.getMaxLines();
    }

    @Override
    public int nextGridMaxColumns() {
        return nextGridProperties.getMaxColumns();
    }
}
