package fr.dmil.hacktris.configuration;

import fr.dmil.hacktris.domain.events.services.PublisherService;
import fr.dmil.hacktris.domain.grids.auxiliary.events.HacktrisHoldGridChangedEvent;
import fr.dmil.hacktris.domain.grids.auxiliary.events.HacktrisNextGridChangedEvent;
import fr.dmil.hacktris.domain.grids.auxiliary.services.HacktrisAuxiliaryGridService;
import fr.dmil.hacktris.domain.grids.auxiliary.services.impl.HacktrisHoldGridServiceImpl;
import fr.dmil.hacktris.domain.grids.auxiliary.services.impl.HacktrisNextGridServiceImpl;
import fr.dmil.hacktris.domain.grids.main.events.HacktrisMainGridChangedEvent;
import fr.dmil.hacktris.domain.grids.main.events.LinesClearedEvent;
import fr.dmil.hacktris.domain.grids.main.services.HacktrisMainGridService;
import fr.dmil.hacktris.domain.grids.main.services.impl.HacktrisMainGridServiceImpl;
import fr.dmil.hacktris.domain.hacktriminos.services.HacktriminoService;
import fr.dmil.hacktris.domain.hacktriminos.services.impl.HacktriminoFactory;
import fr.dmil.hacktris.domain.hacktriminos.services.impl.HacktriminoServiceImpl;
import fr.dmil.hacktris.domain.hacktris.services.HacktrisService;
import fr.dmil.hacktris.domain.hacktris.services.impl.HacktrisServiceImpl;
import fr.dmil.hacktris.domain.properties.services.PropertiesService;
import net.rgielen.fxweaver.core.FxWeaver;
import net.rgielen.fxweaver.spring.SpringFxWeaver;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HacktrisConfiguration {
    @Bean
    HacktriminoFactory hacktriminoFactory() {
        return new HacktriminoFactory();
    }

    @Bean
    HacktriminoService hacktriminoService(
            final PropertiesService propertiesService,
            final HacktriminoFactory hacktriminoFactory) {

        return new HacktriminoServiceImpl(propertiesService, hacktriminoFactory);
    }

    @Bean
    HacktrisMainGridService hacktrisGridService(
            final PublisherService publisherService,
            final PropertiesService propertiesService,
            final LinesClearedEvent linesClearedEvent,
            final HacktrisMainGridChangedEvent hacktrisGridChangedEvent) {

        return new HacktrisMainGridServiceImpl(
                publisherService, propertiesService, linesClearedEvent, hacktrisGridChangedEvent);
    }

    @Bean
    HacktrisAuxiliaryGridService holdHacktriminoGridService(
            final PublisherService publisherService,
            final PropertiesService propertiesService,
            final HacktriminoService hacktriminoService,
            final HacktrisHoldGridChangedEvent hacktrisHoldGridChangedEvent) {

        return new HacktrisHoldGridServiceImpl(
                publisherService,
                propertiesService,
                hacktriminoService,
                hacktrisHoldGridChangedEvent);
    }

    @Bean
    HacktrisAuxiliaryGridService hacktrisNextGridService(
            final PublisherService publisherService,
            final PropertiesService propertiesService,
            final HacktriminoService hacktriminoService,
            final HacktrisNextGridChangedEvent hacktrisNextGridChangedEvent) {

        return new HacktrisNextGridServiceImpl(
                publisherService,
                propertiesService,
                hacktriminoService,
                hacktrisNextGridChangedEvent);
    }

    @Bean
    HacktrisService hacktrisService(
            final HacktriminoService hacktriminoService,
            final PropertiesService propertiesService,
            final HacktrisMainGridService hacktrisMainGridService,
            @Qualifier("holdHacktriminoGridService") final HacktrisAuxiliaryGridService hacktrisHoldGridService,
            @Qualifier("hacktrisNextGridService") final HacktrisAuxiliaryGridService hacktrisNextGridService) {

        return new HacktrisServiceImpl(
                propertiesService,
                hacktriminoService,
                hacktrisMainGridService,
                hacktrisHoldGridService,
                hacktrisNextGridService);
    }

    @Bean
    public FxWeaver fxWeaver(ConfigurableApplicationContext applicationContext) {
        return new SpringFxWeaver(applicationContext);
    }
}
